#!/bin/sh
#
# arch-tag: script to build GNU toolchain
#
# Time-stamp: <2007-05-28 13:41:35 mike>
#
# Script to automate the build process for TT8 version of the GNU
# development tools.  Note that before the GNU tools can be used,
# the Onset libraries must be converted to to COFF format.
#

PATH=/bin:/usr/bin:/usr/local/bin
export PATH

# Source code location.
SRCDIR=${SRCDIR:-`pwd`/src}
# Build directories
BUILDDIR=${BUILDDIR:-`pwd`/build}

# Install location for the development tools.
PREFIX=${PREFIX:-/usr/local}
export PREFIX
export BUILDDIR
export SRCDIR


# This group of variables define the URLs for all of the GNU development
# tools archives.  These versions work, upgrade at your own risk.
GNU_URL="ftp://ftp.gnu.org/gnu"
WGET="wget -nv -nd --passive-ftp"
BINUTILS=binutils-2.15
BINUTILS_URL="${GNU_URL}/binutils/${BINUTILS}.tar.bz2"
GCCVERS=2.95.3
GCC=gcc-${GCCVERS}
GCC_URL="${GNU_URL}/gcc/${GCC}.tar.gz"
NEWLIB=newlib-1.12.0
NEWLIB_URL="ftp://sources.redhat.com/pub/newlib/${NEWLIB}.tar.gz"

# Do not change this
TARGET=m68k-coff

getsources ()
{
    (cd $SRCDIR
     [ ! -f "${BINUTILS}.tar.bz2" ] && $WGET $BINUTILS_URL &&\
       tar xvjf "${BINUTILS}.tar.bz2"
     [ ! -f "${GCC}.tar.gz" ] && $WGET $GCC_URL &&\
       tar xvzf "${GCC}.tar.gz"
     [ ! -f "${NEWLIB}.tar.gz" ] && $WGET $NEWLIB_URL &&\
       tar xvzf "${NEWLIB}.tar.gz"
     )
}

build_binutils ()
{
    (cd $BUILDDIR
     rm -rf build-binutils
     mkdir build-binutils
     cd build-binutils
     ${SRCDIR}/${BINUTILS}/configure --target=${TARGET} --prefix=${PREFIX}
     make all install) 2>&1 | tee ${BUILDDIR}/binutils.log
}

build_bootstrap ()
{
    (cd $BUILDDIR
     rm -rf build-gcc
     mkdir build-gcc
     cd build-gcc
     ${SRCDIR}/${GCC}/configure --target=${TARGET} --prefix=${PREFIX}\
	 --without-headers --with-newlib
     make LANGUAGES="c c++" all-gcc install-gcc) 2>&1 | tee ${BUILDDIR}/bootstrap.log
}

build_gcc ()
{
    (cd $BUILDDIR
     rm -rf build-gcc
     mkdir build-gcc
     cd build-gcc
     ${SRCDIR}/${GCC}/configure --target=${TARGET} --prefix=${PREFIX}
     make LANGUAGES="c c++" all-gcc install-gcc) 2>&1 | tee ${BUILDDIR}/gcc.log
}

post_patch_gcc ()
{
    (cd ${PREFIX}/lib/gcc-lib/${TARGET}/${GCCVERS}
     [ -f ${SRCDIR}/gccpost.patch ] &&\
     patch -p0 < ${SRCDIR}/gccpost.patch || exit 1)
}

build_newlib ()
{
    (cd $BUILDDIR
     rm -rf build-newlib
     mkdir build-newlib
     cd build-newlib
     ${SRCDIR}/${NEWLIB}/configure\
         --target=${TARGET} --prefix=${PREFIX}
     make configure-target-newlib
     make configure-target-libgloss
     rm -rf m68k-coff/m68000/libgloss
     make all-target-newlib install-target-newlib || exit 1
     ) 2>&1 | tee ${BUILDDIR}/newlib.log
}

apply_patches ()
{
    pkg=$1
    level=${2:-0}
    patchdir=`pwd`/patches/$pkg
    (cd $SRCDIR
     for file in ${patchdir}/*.patch
     do
         echo "applying patch $file ..."
         patch -p${level} < $file || exit 1
     done)
}

newlib_patch ()
{
    (cd $SRCDIR/$NEWLIB
     [ -f $SRCDIR/newlib.patch ] &&\
     patch -p1 < $SRCDIR/newlib.patch || exit 1)
}

[ -d $BUILDDIR ] || mkdir -p $BUILDDIR
[ -d $SRCDIR ] || mkdir -p $SRCDIR
#[ -d patches ] && (cd patches && cp -av *.patch $SRCDIR)

getsources
build_binutils
newlib_patch
export PATH=${PREFIX}/bin:${PATH}
build_bootstrap
post_patch_gcc
build_newlib
build_gcc
post_patch_gcc

exit 0

