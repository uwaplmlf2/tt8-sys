/*
** arch-tag: obj68k file converter
**
** Convert an object file in OBJ68k format (as produced by the Aztec C
** m68k cross compiler) to the m68k-COFF format used by the GNU tools.
**
**	Usage: obj68k2coff infile outfile
**
** Copyright (C) 1999 Michael Kenney
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <glib.h>
#include "obj68k2coff.h"

/* COFF section header flags */
#define STYP_NOLOAD	0x02
#define STYP_TEXT	0x20
#define STYP_DATA	0x40
#define STYP_BSS	0x80

/* COFF symbol class flags */
#define C_EXT		2
#define C_STAT		3
#define C_FILE		103

/*
** Symbol information from OBJ68k file
*/
typedef struct {
    guint8		type;		/* section info (see obj68.h) */
    guint8		flags;		/* see obj68k.h */
    guint16		value;		/* symbol location or size */
    guint16		cref;		/* number of code references */
    guint16		dref;		/* number of data references */
    char		*name;		/* symbol name */
    int			index;		/* used for ordering in COFF */
    guint32		vaddr;		/* virtual address */
    guint16		size;		/* size (for global BSS symbols) */
} symbol_t;

/*
** Relocation information.
*/
typedef struct {
    int			type;		/* local or global symbol */
    int			section;	/* text, data, or bss */
    int			sym_idx;	/* symbol table index */
    guint32		addr;		/* location */
    long		offset;
    symbol_t		*sp;
} reloc_t;

/*
** Section description.
*/
typedef struct {
    char	name[8];
    long	nr_bytes;
    long	nr_reloc;
    long	start;
    GByteArray	*bytes;
    GPtrArray	*rlist;
    long	fpointer;
} section_t;

/*
** COFF object file header
*/
typedef struct {
    guint8	magic[2];
    guint8	nscns[2];
    guint8	timdat[4];
    guint8	symptr[4];
    guint8	nsyms[4];
    guint8	opthdr[2];
    guint8	flags[2];
} coff_header_t;

/*
** COFF section header
*/
typedef struct {
    gint8	name[8];
    guint8	paddr[4];
    guint8	vaddr[4];
    guint8	size[4];
    guint8	scnptr[4];
    guint8	relptr[4];
    guint8	lnnoptr[4];
    guint8	nreloc[2];
    guint8	nlnno[2];
    guint8	flags[4];
} section_header_t;

/*
** COFF symbol table entry
*/
typedef struct {
    guint8	name[8];
    guint8	value[4];
    guint8	scnum[2];
    guint8	type[2];
    guint8	sclass;
    guint8	numaux;
} coff_symbol_t;

/*
** Auxiliary symbol table entry for section symbols.
*/
typedef struct {
    guint8	scnlen[4];
    guint8	nreloc[2];
    guint8	nlinno[2];
    guint8	unused[10];
} coff_sectaux_t;

/*
** COFF relocation table entry
*/
typedef struct {
    guint8	vaddr[4];
    guint8	symndx[4];
    guint8	type[2];
} coff_reloc_t;


/*
** These arrays are used to order the symbols for the COFF
** symbol table.
*/
static GPtrArray	*local_syms;
static GPtrArray	*static_syms;
static GPtrArray	*extern_syms;

static const char*
section_name(int code)
{
    switch(code & ST_TYPE)
    {
	case S_ABS:
	    return "absolute";
	case S_CODE:
	    return ".text";
	case S_DATA:
	    return ".data";
	case S_EXPR:
	    return "expression";
	case S_BSS:
	    return ".bss";
	case S_UND:
	    return "undef";
	default:
	    return "unknown";
    }
    
}

static const char*
scope(int code)
{
    code &= 0xff;
    
    if(code & SF_GLOBL)
	return "global";
    else
	return "static";
}

/*
** Read symbols from the input file and append the symbol_t structures
** to 'list'.
*/
static void
read_symbols(int n, FILE *ifp, GPtrArray *list)
{
    symbol_t		*sp;
    guint16		bss_offset;
    int			c, i;
    char		*np;
    char		name[256];

    i = 0;
    bss_offset = 0;
    
    while(i < n)
    {
	sp = g_malloc(sizeof(symbol_t));
	fread(&sp->type, 1L, 1L, ifp);
	fread(&sp->flags, 1L, 1L, ifp);
	fread((char*)&sp->value, 2L, 1L, ifp);
	if(sp->flags & SF_OVSYM)
	    fread((char*)&sp->dref, 2L, 1L, ifp);
	else
	    sp->dref = 0;
	if(sp->flags & SF_HAVCLAS)
	    fread((char*)&sp->cref, 2L, 1L, ifp);
	else
	    sp->cref = 0;

	if((sp->type == S_UND) && (sp->flags & (SF_DATAREF | SF_GLOBL)))
	{
	    /*
	    ** These are global uninitialized values.  The value
	    ** field gives the storage size.  We must convert it
	    ** to an offset.
	    */
	    sp->size = sp->value;
	    sp->value = 0;
	}
	else
	    sp->size = 0;
	
	
	if(sp->flags & S_UNNAMED)
	{
	    /* Give unnamed symbols the name '.N' */
	    sp->name = g_strdup_printf(".%d", i);
	}
	else
	{
	    np = name;
	    while((c = fgetc(ifp)) != '\0' && c != EOF)
		*np++ = c;
	    *np = '\0';
	    if(*name == '_')
		sp->name = g_strdup(name+1);
	    else
		sp->name = g_strdup(name);
	}
	sp->index = i;
	sp->vaddr = 0;

	if((sp->type & ST_TYPE) == S_COMN)
	{
	    sp->type &= ~ST_TYPE;
	    sp->type |= S_BSS;
	}
	
	g_ptr_array_add(list, (gpointer)sp);
	i++;
    }
    
}


void
dump_symbols(GPtrArray *list, FILE *ofp, int n)
{
    int		i;
    symbol_t	*sp;
    
    for(i = 0;i < n;i++)
    {
	sp = g_ptr_array_index(list, i);
	fprintf(ofp, "%s\t\t%s\t0x%04hx\t%s\t%d+%d refs\n", sp->name, 
		section_name((int)sp->type),
		sp->value, scope((int)sp->flags), (int)sp->dref,
		(int)sp->cref);
    }
}

void
dump_reloc(GPtrArray *list, FILE *ofp, int n)
{
    int		i;
    reloc_t	*rp;

    for(i = 0;i < n;i++)
    {
	rp = g_ptr_array_index(list, i);
	fprintf(ofp, "%s 0x%08lx  --> %s %d + %ld\n", section_name(rp->section),
		rp->addr,
		(rp->type == GBLSYM) ? "global" : "local",
		rp->sym_idx, rp->offset);
    }
}

/*
 * Evaluate a relocation expression.  This currently only handles the
 * simple cases of addition and subtraction as I have not found examples
 * of any other expressions.
 */
static void
eval_expr(FILE *ifp, reloc_t *rp)
{
    int		tag, data;
    
    if((tag = fgetc(ifp)) == EOF)
    {
	fprintf(stderr, "Unexpected end-of-file!\n");
	exit(-1);
    }
    
    switch(tag & 0xf0)
    {
	case EXPR:
	    switch(tag & 0x0f)
	    {
		case ADD:
		case SUB:
		    /* Need two operands */
		    eval_expr(ifp, rp);
		    eval_expr(ifp, rp);
		    if(tag == SUB)
			rp->offset *= (-1);
		    break;
		default:
		    fprintf(stderr, "(%ld) Cannot handle expr: 0x%x\n",
			    ftell(ifp), tag);
		    exit(-1);
	    }
	    break;
	
	    /*
	    ** Symbol expression component sets the 'sym_idx' field
	    ** of the data structure.
	    */
	case GBLSYM:
	    data = fgetc(ifp) & 0xff;
	    rp->sym_idx = data*16 + (tag & 0x0f);
	    rp->type = GBLSYM;
	    break;
	case LCLSYM:
	    data = fgetc(ifp) & 0xff;
	    rp->sym_idx = data*16 + (tag & 0x0f);
	    rp->type = LCLSYM;
	    break;

	    /*
	    ** Integer expression component sets the 'offset' field
	    ** of the data structure.
	    */
	case SMLINT:
	    rp->offset = tag & 0x0f;
	    break;
	case MEDINT:
	    data = fgetc(ifp) & 0x7f;
	    rp->offset = data*16 + (tag & 0x0f);
	    break;
	default:
	    fprintf(stderr, "(%ld) Cannot handle expr: 0x%x\n",
		    ftell(ifp), tag);
	    exit(-1);
    }

}

/*
 * Scan the text and data sections of the input file and record all
 * of the references to relocatable symbols.  On return, text_sect
 * and data_sect will contain all of the raw data and relocation
 * information for the respective sections.
 */
int
scan_for_reloc(FILE *ifp, section_t *text_sect, section_t *data_sect)
{
    int			tag, nr_reloc, n;
    reloc_t		*rp;
    section_t		*sect;
    char		buf[16];
    
    text_sect->nr_bytes = 0;
    text_sect->bytes = g_byte_array_new();
    text_sect->nr_reloc = 0;
    text_sect->rlist = g_ptr_array_new();
    
    data_sect->nr_bytes = 0;
    data_sect->bytes = g_byte_array_new();    
    data_sect->nr_reloc = 0;
    data_sect->rlist = g_ptr_array_new();

    sect = text_sect;
    
    nr_reloc = 0;
    
    while((tag = fgetc(ifp)) != EOF)
    {
	if((tag & 0xf0) == ABSDATA)
	{
	    /* Chunk of raw data */
	    n = (tag & 0x0f) + 1;
	    
	    fread(buf, (size_t)n, 1L, ifp);
	    g_byte_array_append(sect->bytes, (guint8*)buf, n);
	    sect->nr_bytes += n;
	    continue;
	}

	if((tag & 0xf0) == SPACE)
	{
	    /* Chunk of null bytes */
	    n = fgetc(ifp) & 0xff;
	    n = n*16 + (tag & 0x0f);
	    while(n--)
	    {
		g_byte_array_append(sect->bytes, (guint8*)"\0", 1);
		sect->nr_bytes++;
	    }
	    continue;
	}
	
	if(tag == USECODE)
	{
	    /* Enter text section */
	    sect = text_sect;
	    continue;
	}
	
	if(tag == USEDATA)
	{
	    /* Enter data section */
	    sect = data_sect;
	    continue;
	}
	
	if(tag == THEEND)
	    break;
	
	if(tag == LEXPR || tag == STARTAD)
	{
	    guint32	marker = 0x55555555;
	    
	    /*
	    ** Evaluate the expression to construct the 32-bit address.
	    ** The address will occupy 4-bytes in the current section.
	    */
	    rp = g_malloc(sizeof(reloc_t));
	    rp->section = (sect == text_sect) ? S_CODE : S_DATA;
	    rp->addr = sect->nr_bytes;
	    rp->offset = 0;
	    rp->sp = NULL;
	    
	    eval_expr(ifp, rp);
	    g_ptr_array_add(sect->rlist, (gpointer)rp);
	    sect->nr_reloc++;
	    
	    /* Use 'marker' as a temporary placeholder */
	    g_byte_array_append(sect->bytes, (guint8*)&marker, 4);
	    sect->nr_bytes += 4;
	    continue;
	}
	
	fprintf(stderr, "Unknown tag 0x%02x at byte %ld\n", tag, ftell(ifp));
	exit(-1);
    }

    /*
    ** Pad sections to 4-byte boundary
    */
    memset(buf, 0, sizeof(buf));
    n = (text_sect->nr_bytes + 3) & ~3;
    n -= text_sect->nr_bytes;
    g_byte_array_append(text_sect->bytes, buf, n);
    text_sect->nr_bytes += n;
    
    n = (data_sect->nr_bytes + 3) & ~3;
    n -= data_sect->nr_bytes;
    g_byte_array_append(data_sect->bytes, buf, n);
    data_sect->nr_bytes += n;
    
    return 1;
}

/*
 * Fill the local_syms, static_syms, and extern_syms GPtrArrays with
 * the symbols in the order expected by COFF.  Calculate the symbol
 * table indicies which will be needed by the relocation entries.
 */
void
order_symbols(GPtrArray *glist, int nr_global, GPtrArray *llist, int nr_local)
{
    int		i, idx, bss_idx;
    symbol_t	*sp;
    
    local_syms = g_ptr_array_new();
    static_syms = g_ptr_array_new();
    extern_syms = g_ptr_array_new();
    
    idx = 2;	/* First two entries are needed for the filename */

    for(i = 0;i < nr_local;i++)
    {
	sp = (symbol_t*)g_ptr_array_index(llist, i);
	sp->index = idx++;
	g_ptr_array_add(local_syms, (gpointer)sp);
    }
    
    /*
    ** Make two passes through the Global list.  One to find the
    ** file-static symbols, the other to find the globals.
    */
    for(i = 0;i < nr_global;i++)
    {
	sp = (symbol_t*)g_ptr_array_index(glist, i);
	if(!(sp->flags & SF_GLOBL))
	{
	    sp->index = idx++;
	    g_ptr_array_add(static_syms, (gpointer)sp);
	}
    }

    idx += 6;	/* leave space for the section symbols */

    bss_idx = idx - 2;
    
    for(i = 0;i < nr_global;i++)
    {
	sp = (symbol_t*)g_ptr_array_index(glist, i);
	if((sp->flags & SF_GLOBL))
	{
	    sp->index = idx++;
	    g_ptr_array_add(extern_syms, (gpointer)sp);
	}
	else
	{
	    /* Set the index for static symbols to bss_idx */
	    if((sp->type & ST_TYPE) == S_BSS)
		sp->index = bss_idx;
	}
	
    }

    /*
    ** Make one last pass through the local symbols and set the
    ** index to the bss index.
    */
    for(i = 0;i < nr_local;i++)
    {
	sp = (symbol_t*)g_ptr_array_index(local_syms, i);
	if((sp->type & ST_TYPE) == S_BSS)
	    sp->index = bss_idx;
    }
    
}

/*
 * Set the proper virtual address in all of the relocation entries.
 */
void
fixup_reloc(section_t *text, section_t *data, GPtrArray *glist, 
	    GPtrArray *llist)
{
    int		i, j, sym_section;
    guint32	vaddr;
    symbol_t	*sp;
    reloc_t	*rp;
    section_t	*sect[2];
    
    sect[0] = text;
    sect[1] = data;
    
    for(j = 0;j < 2;j++)
    {
	for(i = 0;i < sect[j]->nr_reloc;i++)
	{
	    rp = (reloc_t*)g_ptr_array_index((sect[j])->rlist, i);
	    sp = (symbol_t*)g_ptr_array_index((rp->type == GBLSYM ? 
					      glist : llist),
				   rp->sym_idx);
	    sym_section = sp->type & ST_TYPE;
	
	    /*
	    ** Calculate virtual address.  Undefined symbols will get
	    ** a virtual address of zero.
	    */
	    vaddr = sym_section == S_CODE ? text->start :
	      (sym_section == S_DATA ? data->start :
	       (sym_section == S_BSS ? data->start + data->nr_bytes : 0));
	    vaddr += sp->value;
	    vaddr += rp->offset;
	    
	
	    /*
	    ** Write the new address into the section data, MSB first.
	    */
	    sect[j]->bytes->data[rp->addr]   = (guint8)(vaddr >> 24);
	    sect[j]->bytes->data[rp->addr+1] = (guint8)(vaddr >> 16);
	    sect[j]->bytes->data[rp->addr+2] = (guint8)(vaddr >> 8);
	    sect[j]->bytes->data[rp->addr+3] = (guint8)vaddr;
	
	    /* Save link to symbol for later use */
	    sp->vaddr = vaddr;
	    rp->sp = sp;
	}
    }
    
    /*
    ** Find the symbols without references and set the virtual address
    */
    for(i = 0;i < llist->len;i++)
    {
	sp = (symbol_t*)g_ptr_array_index(llist, i);
	if(sp->cref == 0 && sp->dref == 0)
	{
	    sym_section = sp->type & ST_TYPE;
	
	    /*
	    ** Calculate virtual address.
	    */
	    sp->vaddr = sym_section == S_CODE ? text->start :
	      (sym_section == S_DATA ? data->start :
	       (sym_section == S_BSS ? data->start + data->nr_bytes : 0));
	    sp->vaddr += sp->value;
	}
    }

    for(i = 0;i < glist->len;i++)
    {
	sp = (symbol_t*)g_ptr_array_index(glist, i);
	if(sp->cref == 0 && sp->dref == 0)
	{
	    sym_section = sp->type & ST_TYPE;
	
	    /*
	    ** Calculate virtual address.
	    */
	    sp->vaddr = sym_section == S_CODE ? text->start :
	      (sym_section == S_DATA ? data->start :
	       (sym_section == S_BSS ? data->start + data->nr_bytes : 0));
	    sp->vaddr += sp->value;
	}
    }
    
}

void
write_coff_header(FILE *ofp)
{
    coff_header_t	hdr;
    time_t		now = time(0);
    int			i;
    
    hdr.magic[0] = 0x01;
    hdr.magic[1] = 0x50;
    hdr.nscns[0] = 0;
    hdr.nscns[1] = 3;
    hdr.timdat[0] = now >> 24;
    hdr.timdat[1] = now >> 16;
    hdr.timdat[2] = now >> 8;
    hdr.timdat[3] = now;

    /*
    ** Initialize symbol table information to zero.  These fields will
    ** be filled-in later.
    */
    for(i = 0;i < 4;i++)
    {
	hdr.symptr[i] = 0;
	hdr.nsyms[i] = 0;
    }
    
    hdr.opthdr[0] = 0;
    hdr.opthdr[1] = 0;
    hdr.flags[0] = 0x02;
    hdr.flags[1] = 0x04;
    
    fwrite((char*)&hdr, sizeof(hdr), (size_t)1, ofp);
    
}

void
write_section_header(FILE *ofp, section_t *sect, guint32 flags)
{
    section_header_t	hdr;
    int			i;
    
    memset(hdr.name, 0, (size_t)8);
    strncpy(hdr.name, sect->name, 8);
    
    hdr.paddr[0] = hdr.vaddr[0] = sect->start >> 24;
    hdr.paddr[1] = hdr.vaddr[1] = sect->start >> 16;
    hdr.paddr[2] = hdr.vaddr[2] = sect->start >> 8;
    hdr.paddr[3] = hdr.vaddr[3] = sect->start;
    hdr.size[0] = sect->nr_bytes >> 24;
    hdr.size[1] = sect->nr_bytes >> 16;
    hdr.size[2] = sect->nr_bytes >> 8;
    hdr.size[3] = sect->nr_bytes;
    
    for(i = 0;i < 4;i++)
    {
	hdr.scnptr[i] = 0;
	hdr.relptr[i] = 0;
	hdr.lnnoptr[i] = 0;
    }
    
    hdr.nreloc[0] = sect->nr_reloc >> 8;
    hdr.nreloc[1] = sect->nr_reloc;
    hdr.nlnno[0] = 0;
    hdr.nlnno[1] = 0;
    
    hdr.flags[0] = flags >> 24;
    hdr.flags[1] = flags >> 16;
    hdr.flags[2] = flags >> 8;
    hdr.flags[3] = flags;
    
    sect->fpointer = ftell(ofp);
    fwrite((char*)&hdr, sizeof(hdr), (size_t)1, ofp);
    
}

void
write_section_data(FILE *ofp, section_t* sect)
{
    long	fpos;
    guint8	buf[4];
    
    /*
    ** Rewind the file back to the section header and record the
    ** file position of the raw data.
    */
    fpos = ftell(ofp);
    fseek(ofp, sect->fpointer+20, SEEK_SET);
    buf[0] = fpos >> 24;
    buf[1] = fpos >> 16;
    buf[2] = fpos >> 8;
    buf[3] = fpos;
    fwrite(buf, sizeof(buf), 1L, ofp);

    /*
    ** Return to the saved position and write the data.
    */
    fseek(ofp, fpos, SEEK_SET);
    fwrite(sect->bytes->data, 1L, sect->nr_bytes, ofp);
}

void
write_reloc_data(FILE *ofp, section_t *sect)
{
    int		i;
    long	fpos;
    reloc_t	*rp;
    guint32	addr;
    guint8	buf[4];
    coff_reloc_t	reloc;

    /*
    ** Rewind the file back to the section header and record the
    ** file position of the relocation data.
    */
    if(sect->nr_reloc > 0)
    {
	fpos = ftell(ofp);
	fseek(ofp, sect->fpointer+24, SEEK_SET);
	buf[0] = fpos >> 24;
	buf[1] = fpos >> 16;
	buf[2] = fpos >> 8;
	buf[3] = fpos;
	fwrite(buf, sizeof(buf), 1L, ofp);
	fseek(ofp, fpos, SEEK_SET);
    }
    

    for(i = 0;i < sect->nr_reloc;i++)
    {
	rp = (reloc_t*)g_ptr_array_index(sect->rlist, i);
	addr = rp->addr + sect->start;
	
	reloc.vaddr[0] = addr >> 24;
	reloc.vaddr[1] = addr >> 16;
	reloc.vaddr[2] = addr >> 8;
	reloc.vaddr[3] = addr;
	reloc.symndx[0] = rp->sp->index >> 24;
	reloc.symndx[1] = rp->sp->index >> 16;
	reloc.symndx[2] = rp->sp->index >> 8;
	reloc.symndx[3] = rp->sp->index;
	reloc.type[0] = 0;
	reloc.type[1] = 0x11;
	
	fwrite((char*)&reloc, sizeof(reloc), (size_t)1, ofp);
    }
}

static int
write_symbols(FILE *ofp, GPtrArray *list, GPtrArray *strings,
	      long *string_len)
{
    int			namelen, i, nr_syms, n;
    symbol_t		*sp;
    coff_symbol_t	sym;

    nr_syms = 0;
    
    for(i = 0;i < list->len;i++)
    {	
	memset(&sym, 0, sizeof(sym));
	sp = (symbol_t*)g_ptr_array_index(list, i);
	if((namelen = strlen(sp->name)) > 7)
	{
	    g_ptr_array_add(strings, sp->name);

	    n = *string_len + 4;
	    
	    sym.name[4] = n >> 24;
	    sym.name[5] = n >> 16;
	    sym.name[6] = n >> 8;
	    sym.name[7] = n;
	    *string_len += (namelen + 1);
	}
	else
	    strcpy(sym.name, sp->name);

	sym.value[0] = sp->vaddr >> 24;
	sym.value[1] = sp->vaddr >> 16;
	sym.value[2] = sp->vaddr >> 8;
	sym.value[3] = sp->vaddr;

	sym.scnum[0] = 0;
	
	switch(sp->type & ST_TYPE)
	{
	    case S_CODE:
		sym.scnum[1] = 1;
		break;
	    case S_DATA:
		sym.scnum[1] = 2;
		break;
	    case S_BSS:
	    case S_COMN:
		sym.scnum[1] = 3;
		break;
	    case S_UND:
		sym.value[0] = 0;
		sym.value[1] = 0;
		sym.value[2] = sp->size >> 8;
		sym.value[3] = sp->size;
	    default:
		sym.scnum[1] = 0;
		break;
	}

	sym.sclass = (sp->flags & SF_GLOBL) ?  C_EXT : C_STAT;
	sym.numaux = 0;
	fwrite((char*)&sym, sizeof(sym), 1L, ofp);
	nr_syms++;
    }

    return nr_syms;
}

static void
write_section_symbol(FILE *ofp, section_t *sect, int snum)
{
    coff_symbol_t	sym;
    coff_sectaux_t	aux;
    
    memset(&sym, 0, sizeof(sym));
    memset(&aux, 0, sizeof(aux));

    strcpy(sym.name, sect->name);

    sym.value[0] = sect->start >> 24;
    sym.value[1] = sect->start >> 16;
    sym.value[2] = sect->start >> 8;
    sym.value[3] = sect->start;

    sym.scnum[1] = snum;
    sym.sclass = C_STAT;
    sym.numaux = 1;

    aux.scnlen[0] = sect->nr_bytes >> 24;
    aux.scnlen[1] = sect->nr_bytes >> 16;
    aux.scnlen[2] = sect->nr_bytes >> 8;
    aux.scnlen[3] = sect->nr_bytes;
    
    aux.nreloc[0] = sect->nr_reloc >> 8;
    aux.nreloc[1] = sect->nr_reloc;
    
    fwrite((char*)&sym, sizeof(sym), (size_t)1, ofp);
    fwrite((char*)&aux, sizeof(aux), (size_t)1, ofp);
    
}

void
write_symbol_table(FILE *ofp, const char *fname, section_t *text, 
		   section_t *data, section_t *bss)
{
    long		fpos, string_len, n;
    int			i;
    GPtrArray		*strings;
    coff_symbol_t	sym;
    guint8		buf[4];
    
    /* Remember the starting point */
    fpos = ftell(ofp);

    strings = g_ptr_array_new();
    string_len = 0;
    
    /*
    ** First entry is the filename.
    */
    memset(&sym, 0, sizeof(sym));
    strcpy(sym.name, ".file");
    sym.scnum[0] = 0xff;
    sym.scnum[1] = 0xfe;
    sym.sclass = C_FILE;
    sym.numaux = 1;
    fwrite((char*)&sym, sizeof(sym), 1L, ofp);
    memset(&sym, 0, sizeof(sym));
    strncpy((char*)&sym, fname, 14);
    fwrite((char*)&sym, sizeof(sym), 1L, ofp);

    n = 2;
    n += write_symbols(ofp, local_syms, strings, &string_len);
    n += write_symbols(ofp, static_syms, strings, &string_len);

    /*
    ** Write the three section entries
    */
    write_section_symbol(ofp, text, 1);
    write_section_symbol(ofp, data, 2);
    write_section_symbol(ofp, bss, 3);
    
    n += 6;
    n += write_symbols(ofp, extern_syms, strings, &string_len);
    
    /*
    ** Write the string table
    */
    string_len += 4;
    
    buf[0] = string_len >> 24;
    buf[1] = string_len >> 16;
    buf[2] = string_len >> 8;
    buf[3] = string_len;
    fwrite(buf, (size_t)1, (size_t)4, ofp);
    
    for(i = 0;i < strings->len;i++)
    {
	char	*str;
	str = g_ptr_array_index(strings, i);
	fwrite(str, 1L, strlen(str)+1, ofp);
    }

    fflush(ofp);

    /*
    ** Rewind and update the file header
    */
    fseek(ofp, 8L, SEEK_SET);
    buf[0] = fpos >> 24;
    buf[1] = fpos >> 16;
    buf[2] = fpos >> 8;
    buf[3] = fpos;
    fwrite(buf, (size_t)1, (size_t)4, ofp);
    buf[0] = n >> 24;
    buf[1] = n >> 16;
    buf[2] = n >> 8;
    buf[3] = n;
    fwrite(buf, (size_t)1, (size_t)4, ofp);
    
    g_ptr_array_free(strings, FALSE);
    
}

int
main(int ac, char *av[])
{
    int			i;
    GPtrArray		*global, *local;
    FILE		*ifp, *ofp;
    struct module	header;
    section_t		text, data, bss;
    
    if(ac < 2)
    {
	fprintf(stderr, "Usage: %s objfile coff_file\n", av[0]);
	exit(-1);
    }
    
    if((ifp = fopen(av[1], "r")) == NULL)
    {
	fprintf(stderr, "Cannot open file %s\n", av[1]);
	exit(-1);
    }
    
    if((ofp = fopen(av[2], "w")) == NULL)
    {
	fprintf(stderr, "Cannot open file %s\n", av[2]);
	exit(-1);
    }

    fread((char*)&header.m_magic, 2L, 1L, ifp);
    if(header.m_magic != M_MAGIC)
    {
	fprintf(stderr, "Not a valid object file (0x%04hx)\n", header.m_magic);
	exit(-1);
    }
    
    fread(&header.m_name, sizeof(header.m_name), 1L, ifp);
    fread((char*)&header.m_code, 4L, 1L, ifp);
    fread((char*)&header.m_data, 4L, 1L, ifp);
    fread((char*)&header.m_static, 4L, 1L, ifp);
    
    fread((char*)&header.m_nglobal, 2L, 1L, ifp);
    fread((char*)&header.m_nlocal, 2L, 1L, ifp);
    fread((char*)&header.m_recs, 2L, 1L, ifp);
    fread((char*)&header.m_global, 2L, 1L, ifp);
    fread((char*)&header.m_local, 2L, 1L, ifp);
    fread((char*)&header.m_next, 2L, 1L, ifp);
    fread((char*)&header.m_nfix, 2L, 1L, ifp);
    
    fprintf(stderr, "Reading global symbols\n");
    fseek(ifp, M_SHIFT(header.m_global), SEEK_SET);
    global = g_ptr_array_new();
    read_symbols((int)header.m_nglobal, ifp, global);

    /*
    ** Adjust the offsets for the global uninitialized symbols
    */
    for(i = 0;i < header.m_nglobal;i++)
    {
	symbol_t	*sp;
	
	sp = g_ptr_array_index(global, i);
	if(sp->flags == (SF_DATAREF | SF_GLOBL))
	    sp->value += header.m_static;
    }
    
	    
    fprintf(stderr, "Reading local symbols\n");
    fseek(ifp, M_SHIFT(header.m_local), SEEK_SET);
    local = g_ptr_array_new();
    read_symbols((int)header.m_nlocal, ifp, local);

    fprintf(stderr, "Reading relocation info\n");
    fseek(ifp, M_SHIFT(header.m_recs), SEEK_SET);    

    memset((char*)&bss, 0, sizeof(bss));
    
    strcpy(text.name, ".text");
    strcpy(data.name, ".data");
    strcpy(bss.name, ".bss");
    
    
    scan_for_reloc(ifp, &text, &data);

    text.start = 0;
    data.start = text.nr_bytes;
    bss.start = data.start + data.nr_bytes;
    bss.nr_bytes = header.m_static;

    order_symbols(global, (int)header.m_nglobal, local, (int)header.m_nlocal);
    fixup_reloc(&text, &data, global, local);
    write_coff_header(ofp);
    write_section_header(ofp, &text, STYP_TEXT);
    write_section_header(ofp, &data, STYP_DATA);
    write_section_header(ofp, &bss, STYP_BSS | STYP_NOLOAD);
    write_section_data(ofp, &text);
    write_section_data(ofp, &data);
    write_reloc_data(ofp, &text);
    write_reloc_data(ofp, &data);
    write_symbol_table(ofp, av[1], &text, &data, &bss);
    
    return 0;
}
