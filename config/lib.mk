#
# arch-tag: makefile component to build library archives
#

ifndef mklib
mklib := 1

$(THISLIB): $(LIBOBJECTS)
	-rm -f $(THISLIB)
	$(AR) cru $(THISLIB) $(LIBOBJECTS)
	$(RANLIB) $(THISLIB)

ifdef INSTALL_LIB
install: $(THISLIB) $(LIBEXTRAS)
	mkdir -p $(INSTALL_LIB)
	test -d $(INSTALL_LIB) && cp -v $^ $(INSTALL_LIB)
endif

ifdef INSTALL_HEADERS
install-headers: $(HEADERS)
	mkdir -p $(INSTALL_HEADERS)
	test -d $(INSTALL_HEADERS) && cp -v $(HEADERS) $(INSTALL_HEADERS)
endif

endif
