#
# arch-tag: common makefile components for TT8 code
#
ifndef tt8cross
tt8cross := 1

AR = m68k-coff-ar
CC = m68k-coff-gcc -Os -mcpu32 -mshort -fshort-enums -fno-builtin\
   -D__TT8__ -D__PICODOS__
LD = m68k-coff-ld
AS = m68k-coff-as -mcpu32
OBJCOPY = m68k-coff-objcopy
RANLIB = m68k-coff-ranlib

LINKFILE ?= tt8-ram.ld

COMPILE = $(CC) $(DEFS) $(INCLUDES) $(CPPFLAGS) $(CFLAGS)
CCLD = $(CC)
LINK = $(CCLD) $(CFLAGS) -T $(LINKFILE) $(LDFLAGS) -o $@

endif
