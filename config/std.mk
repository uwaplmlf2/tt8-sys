#
# arch-tag: common makefile components for native compiling
#
ifndef std
std := 1

AR = ar
CC = gcc
RANLIB = ranlib

COMPILE = $(CC) $(DEFS) $(INCLUDES) $(CPPFLAGS) $(CFLAGS)
CCLD = $(CC)
LINK = $(CCLD) $(CFLAGS) $(LDFLAGS) -o $@

endif
