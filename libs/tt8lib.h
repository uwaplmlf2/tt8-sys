/*******************************************************************************
**	tt8lib.h -- Tattletale Model 8 Library Function Prototypes and Definitions
**
**          Copyright 1994 ONSET Computer Corp.  All rights reserved.
**	
**	Thursday, February 23, 1994	--jhg
**	
**------------------------------------------------------------------------------
**	Modified Friday, December 9, 1994 --jhg
**		add note to don't use TSerOutQEmpty macro (wrong truth sense)
**		create new macro TSerOutQBytes to return count remaining in queue
**	
**------------------------------------------------------------------------------
**	Modified Sunday, April 3, 1994	--jhg
**		added new functions:
**			void VRegSelect(enum { v5, v3 } vreg);
**			void SerShutDown(short immed, short autowake);
**			void SerActivate(void);
**	
**------------------------------------------------------------------------------
**	Modified Sunday, April 24, 1994 --jhg
**		tt8init.c:
**			Changed VBR from application global variable to constant at 0x2FC000
**			to allow better inter-operability with MONTT8.
**		
**			Added conditionals for installing TPU and SANE stuff for THINK C
**			YLoader program.
**		
**		tt8pic.c:
**			Moved more of the functions into the area defined as MINIMUM_PIC_LIB,
**			again primarily for THINK C YLoader.
**		
**		tt8tpu.c:
**			Restored default Onset TPU image to embedded table, which is user-
**			overridden if code exists at $1800.
**			
**			Changed context of < -1 parameter to mean perform no initialization.
**		
**		_exit.c
**			Removed call to fflush(stdout) which forced linkage with the rest
**			of the ANSI C library.
**------------------------------------------------------------------------------
**	Modified Saturday, July 9, 1994 --jhg
**		add support for 1 M PSRAM in tt8init()
**			
**		modified start code with SSR bug causing flash programs to die.
**			
**		recompile with newer Aztec C 5.2b 06/95
**			
**		use full optimizations in makefile
**			
**		added Aztec version of 0x200000 dump on generic error
**			
**		change exit() from Reset() to ResetToMon()
**
**------------------------------------------------------------------------------
**	Modified Saturday, July 9, 1994 --jhg
**		Removed (mostly commented out) code for PSRAM, but kept stuff needed 
**		for 1M SRAM.
**	
**		Rebuilt library, old version had weird problem setting wait states?
**	
**		Changed InitSIM to use FLASH_WAITS and SRAM_WAITS instead of constant
**		values 1 and 1. (wasn't working for Aztec anyway).
**	
**		Added InfoTT8 function to return information about the Model 8 
**		components and software. This uses a new (non-public) GetFlashInfo()
**		function in tt8misc.c
**	
**		Fixed TensMilliSecs() function which was really returning tenths of mS.
**
**------------------------------------------------------------------------------
**	Modified Saturday, March 16, 1996 --jhgodley@periph.com
**
**		Many modifications to support MotoCross and the Metrowerks compilers.
**
**		Add definitions for auxiliary RS-232 signals TT8TUIN and TT8TUOUT
**
**		Added global field __TT8LIB_DATE__ to identify the library build date 
**		and display it in InfoTT8.
**
**		Added decoded exception information showing the PC, SR, format, vector
**		and stack at the time of the fault. (Got tired of searching for the
**		translation information in the manual appendix).
**
**		Many minor modifications to satisfy the extended error checking from
**		the Metrowerks CodeWarrior C++ compiler (final build used just C).
**		
**------------------------------------------------------------------------------
**	Modified Saturday, August 31, 1996 --jhgodley@periph.com
**
**		Changed the default initialization of CS10/ECLK to force it to a chip
**		select instead of ECLK to allow devices connected to CS10 at power up. 
**		This mates with a similar change to the TOM8 monitor 1.08.
**	
**		Changed time_t definition to temporary just for duration of this header.
**	
**------------------------------------------------------------------------------
**	Modified Sunday, September 29, 1996 --jhgodley@periph.com
**	
**		Changed constant LIB_VERSION to global long __TT8LIB_VERS__ to allow
**		synchronization with other libraries. Also added library globals...
**		__TT8CLIB_VERS__, __TT8CLIB_DATE__, __TT8MLIB_VERS__, __TT8MLIB_DATE__,
**		to the C and Math libraries for the same reason. We can't reference  
**		them directly without forcing users to link in both libraries, but 
**		we've added members to the end of the TT8info structure <tt8lib.h> 
**		that can be filled and referenced by the user.
**	
**------------------------------------------------------------------------------
**	Modified, 3.01, Friday, November 15, 1996 --jhgodley@periph.com
**
**	1.	tt8atod.c:Max186PowerUp() Replaced numeric constant 20 for pUpDelay
**		with symbolic constant POWER_UP_DELAY_MS defined to 2, which is more
**		appropriate according to MAX186 data sheet.
**	
**	2.	tt8serio.c:SerInterrupt() Added the capability to intercept and/or 
**		filter incoming serial data at the interrupt handler. There's now
**		a new setup call SerSetFilterFunct(), with which you install a 
**		handler to scan serial data before it gets into the ring buffer and
**		optionally replace the data or even force it to be skipped. The new
**		function prototype and brief description is in this file, and there's
**		a new example program showing its use.
**	
**	3.	tt8pic.c:PicSetTime() Fixed a long standing bug where setting the 
**		seconds did nothing to change the ticks count, which could lead to 
**		almost immediate time change after setting the time and assuming a
**		full second would pass before the next change.
**	
**	4.	tt8tuart.c:TSerGetQueue() Added this function to allow more efficient
**		access to the buffer contents. This, combined with TSerInQBytes() 
**		(also know as TSerByteAvail()) and TSerInFlush() provide greater
**		flexibility in handling tpu serial data.
**	
**------------------------------------------------------------------------------
**	Modified, 3.02, Monday, January 20, 1997 --jhgodley@periph.com
**
**		Older versions (pre-5.2C) of the Aztec compiler wouldn't allow the
**		double slash comments for preprocessor declarations. Changed then
**		all to slash-asterisk pairs.
**	
**------------------------------------------------------------------------------
**	Modified, 3.03, Saturday, January 25, 1997 --jhgodley@periph.com
**
**		Added guard code to TSerOpen to prevent divide by zero exceptions
**		when zero is passed as the baud argument. Also added tsZeroBaudErr
**		error code to enumerated list. 
**	
**------------------------------------------------------------------------------
**	Modified, 3.04, Monday, February 24, 1997 --jhgodley@periph.com
**	
**	1.	Added initialization code to InitTT8() which calls UeeSize() to force
**		a determination of the actual serial eeprom size rather than just
**		defaulting to 2K.
**	
**	2.	Added additional code to tt8pic.c:PicSetTime() which forces off the
**		the alarm interrupt whenever you set a new time.
**	
**------------------------------------------------------------------------------
**	Modified, 3.05, Tuesday, March 4, 1997 --jhgodley@periph.com
**	
**	1.	Removed the code in _isatty() which improperly set errno to EBADF
**	
**	2.	Added InstallDefaultHandler() function to allow overriding the default
**		exit behavior of the generic interrupt/exception handler which prints
**		out a terse summary of the principal registers, then exits to the TOM8
**		via a PIC call that keeps IRQ3 asserted (LED on) and the TT8 drawing
**		lots of power. 
**		
**		InstallDefaultHandler() accepts a single vfptr parameter which is a
**		pointer to C function which gets control after the generic handler has 
**		reset the SIM registers to default values (same as just after the 
**		InitTT8 call) and copied all of the registers to the global postmortem
**		memory at 0x2C0000.
**		
**		The simplest replacement function would just call Reset() and hope
**		that whatever caused the failure goes away. In some remote battery 
**		powered applications, this may be preferable to just letting the TT8
**		draw power indefinitely.
**		
**		Be aware that replacing the generic handler doesn't fix anything. It
**		just masks a programming problem (division by zero, wild pointer, etc.)
**		that will continue to haunt your program if not rigorously pursued.
**	
**	3.	Added AtoDReqRepeat() function which, when passed a non-zero value,
**		flags the MAX-186 and QSPI solely for A-D operations, and allows
**		future calls to AtoDReadWord() or AtoDReadMilliVolts() to skip many
**		time consuming setup steps when it's known the A-D will be run in a
**		repeated mode. At 16MHz, this drops the execution time from approx.
**		150uS down to 50uS.
**		
**		This flag is off by default, and turned off by Max186PowerDown() or
**		a call to AtoDReqRepeat() with a zero value. Existing programs are
**		unaffected by this addition as it requires an explicit call for use.
**		
**------------------------------------------------------------------------------
**	Modified, 3.06, Friday, April 18, 1997 --jhgodley@periph.com
**	
**	1.	Fixed a bug in DelayMilliSecs() which caused this function to ignore
**		changes to the time base from calls to SetTickRate();
**		
**------------------------------------------------------------------------------
**	Modified, 3.07, Thursday, June 19, 1997 --jhgodley@periph.com
**	
**	1.	Fixed a long standing bug in TSerPutByte() which caused the Model 8 to
**		freeze when a stream of outgoing TPU UART characters reached the buffer 
**		size before the queued data could be shifted out. 
**		
**------------------------------------------------------------------------------
**	Modified, 3.08, Sunday, July 20, 1997 --jhgodley@periph.com
**	
**	1.	Fixed a bug in InfoTT8 which printed the decimal portion of the version
**		number without the leading zero for values below 10. 
**		
**	2.	Changed the execution order of the generic exception handler (see the	
**		notes above for 3.05-2) to give even more control to the user's custom
**		handler. Where before, the generic handler performed several repeat
**		initializations, the new generic handler just captures the machine
**		context, switches to a small (400 byte) private stack, then calls the
**		user handler (if installed). If you intend to attempt some kind of 
**		recovery, your handler will have had to make earlier provisions to
**		capture the stack pointer (perhaps with setjmp/longjmp).
**		
**	3.	Moved the typedefs and identifiers from private declaration in the	
**		library source to public declaration in <tt8lib.h> to allow user
**		access to the machine state stored at the base of ram. You can access
**		all of the captured state using the ExcRegs struct and by defining a
**		a pointer to the exception stack frame as shown below.
**		
**			ExcInfoPtr	ep = (ExcInfoPtr) BASEADDR(* (uspv) RamOEBase);
**		
**		From that, the PC is at ep->esf.ePC, the vector at ep->esf.vectofs, and
**		so on (see the ExcStackFrame struct definition below). From ExcRegs, you
**		can get at the machine registers. For example, the stack pointer is at
**		ExcRegs.sysRegs[rA7], and from that, you can crawl the stack.
**		
**------------------------------------------------------------------------------
**	Modified, 3.09, Tuesday, July 29, 1997 --jhgodley@periph.com
**		
**	1.	Changed the text in item 2 of the 3.08 modifications to mention that the 
**		generic handler switches to a private 400 byte stack.
**		
**	2.	Max186ReadChan() has a long-standing bug where a request to operate in
**		bipolar mode changes the context of the chan parameter from a simple 
**		channel number selector (0..7) to the SEL bits sent to the MAX186 in
**		the control register. This should be the way the function operates when 
**		differential mode is requested. Even though this function only exists
**		as an appnote in the Model 8 manual, we can't change this now without
**		possibly breaking existing code, so we've added a new function that
**		perform similarly, but with this bug corrected. The new function can
**		also be defined in a macro to replace the faulty version.
**	
**		short Max186ConvertChannel(short chan, short bipolar, 
**				short diff, short pdmode);
**	
**	3.	While we're at it, we've also added a bipolar version of the popular
**		AtoDReadWord() function called AtoDReadBipolar(). It calls the new
**		Max186ConvertChannel() function and returns a signed integer in the
**		range 0x7FF0 (32752 at 2.048V) to 0x8000 (-32768 at -2.048V). Dividing
**		by 16 (>> 4) converts the result back to millivolts.
**	
**		short AtoDReadBipolar(short chan);	// bipolar version of AtoDReadWord
**	
**	4.	Rebuilt TT8.LIB, CL16TT8.LIB, and ML16TT8.LIB using the latest 5.2d
**		versions of the Aztec C compiler.
**		
**------------------------------------------------------------------------------
**	Modified, 3.10, Wednesday, July 30, 1997 --jhgodley@periph.com
**		
**	1.	Added the new function VRegSwitchTo() which flash applications should
**		use instead of VRegSelect() to switch the regulated voltage between 5V
**		and 3.3V. This new function provides a work-around for data line
**		glitching that can occur with certain Atmel flash parts when the
**		supply voltage transitions at about 3.8V. 
**		
**			short VRegSwitchTo(short vreg, ushort loopdelay, ushort ruptmask);
**		 
**		VRegSwitchTo() lets you specify a delay from the time of the voltage
**		switch until execution resumes, and also lets you specify an interrupt
**		mask to prevent external interrupts from possibly forcing flash execution
**		during the critical transition period. The delay value can range from 0
**		to 65535 and the actual delay time varies with operating frequency as
**		described by the following formula: 
**		
**			delay in milliseconds = 8 * loopdelay / fKHz
**		
**		We recommend a delay of 64000 (32mS@16MHz, 533ms at 960kHz) and a mask
**		level of NO_RUPTS_MASK (7) for any programs running from flash. Longer
**		delays are required for slower frequencies since their lower operating
**		currents lengthen the transition time. Note that since the function
**		requires communication with the PIC, it will not attempt a voltage
**		switch at frequencies below 960kHz or above 16MHz.
**		
**		Many thanks to Dave Fitzpatrick from McLane Research for pointing out
**		the need for this fix.
**		
**------------------------------------------------------------------------------
**	Modified, 3.11, Tuesday, October 7, 1997 --jhgodley@periph.com
**		
**	1.	Changed release number to reflect Onset/Pii build and distribution of 
**		3.10 beta sources.
**		
**------------------------------------------------------------------------------
**	Modified, 3.12, Friday, November 21, 1997 --jhgodley@periph.com
**		
**	1.	Made a change to the one of the internal low-level PIC functions that
**		will enhance the reliability of applications that make heavy use of 
**		interrupts. This directly affects almost every PIC call, and therefore
**		indirectly affects all of the time and eeprom functions. The only change
**		you may see is an additional 70uS (16MHz) interrupt response latency if
**		an interrupt hits in the middle of a PIC call. The latency extends to 
**		90uS at 8MHz, and 300uS at 1.28MHz.
**		
**		There are few (if any) Model 8 applications that will be adversely
**		affected by a very infrequent additional 70uS latency, but if you have
**		a high speed interrupt paced application that behaves differently after
**		updating to the new libraries, you should contact one of the engineers
**		at Onset, then turn this protection off with the following call:
**		
**			PicIRQHoldoffs(0);		// zero turns protection off, 1 enables
**
*******************************************************************************/

#ifndef		__tt8lib_H
#define		__tt8lib_H

#include	<tt8.h>
#include	<tat332.h>

#ifndef time_t
  #define	time_t	unsigned long
  #define	tt8_time_t	/* so we can undefine time_t */
#endif

/*
**	ANALOG TO DIGITAL
*/
void AtoDReqRepeat(short req);		/* 3.05, 97/03/04--jhg */
short AtoDReadWord(short chan);
#define AtoDtoMilliVolts(adval)		((adval) >> 3)
#define AtoDReadMilliVolts(chan)	(AtoDtoMilliVolts(AtoDReadWord(chan)))

enum pdMode {	pdFull, pdFast, pdIntClk, pdExtClk };
enum shdnCompMode {	extComp = -1 /* floating */, shutDown = 1, intComp = 0 };
	
void Max186PowerUp(void);
void Max186PowerDown(void);
void Max186Setup(short pdMode, short shdnCompMode);

/* 3.10, 97/07/29--jhg */
short Max186ConvertChannel(short chan, short bipolar, short diff, short pdmode);
short AtoDReadBipolar(short chan);

/*
**	CRC
*/
ushort CalcCRC(uchar *tptr, ulong count, ushort crc);
ushort UpdateCRC(ushort b, ushort crc);

/*
**	EXCEPTIONS (INTERRUPTS)
*/
typedef struct 
	{
	ushort		eSR;			/* status register */
	ulong		ePC;			/* program counter */
	ushort		vectofs;		/* format 15:12, vector 11:0 */
	ushort		states[8];		/* other processing state information */
	} 	ExcStackFrame;

typedef struct
	{
	ushort	preamble[4];	/* 00: 48E7 FFFE	MOVEM.L D0-D7/A0-A6,-(A7)	*/
							/* 02: 41EF 003C	LEA     60(A7),A0			*/
	ushort	jump;			/* 04: 4EB9			JSR.L   $ABCD1234			*/
	ulong	target;			/* 05: ABCD 1234	  ABS ADDRESS				*/
	ushort	postamble[3];	/* 07: 4CDF 7FFF	MOVEM.L (A7)+,D0-D7/A0-A6	*/
							/* 09: 4E73			RTE     					*/
	}	ExcCFrame, *ExcCFramePtr;				/*	approx 12uS @ 16MHz	  	*/

enum
	{
	Reset_Stack_Pointer = 0,
	Reset_Program_Counter,
	Bus_Error,
	Address_Error,
	Illegal_Instruction,
	Zero_Divide,
	CHK_Instructions,
	TRAP_Instructions,
	Privilege_Violation,
	Trace,
	Line_1010_Emulation,
	Line_1111_Emulation,
	Hardware_Breakpoint,
	Reserved_for_Coprocessor,
	Format_Error,
	Unitialized_Interrupt,	
	Spurious_Interrupt = 24,
	Level_1_Interrupt,
	Level_2_Interrupt,
	Level_3_Interrupt,
	Level_4_Interrupt,
	Level_5_Interrupt,
	Level_6_Interrupt,
	Level_7_Interrupt
	};

enum
	{
	rPC, rSR, rUSP, rSSP, rVBR, rSFC, rDFC, rXXX,
	rD0, rD1, rD2, rD3, rD4, rD5, rD6, rD7,
	rA0, rA1, rA2, rA3, rA4, rA5, rA6, rA7
	};

typedef struct _Regs
	{
	ulong	sysRegs[8];			/* system registers */
	ulong	dataRegs[8];		/* data registers */
	ulong	addrRegs[8];		/* address registers */
	}	Regs, *RegsPtr;

typedef struct _ExcInfo
	{
	Regs			regs;
	ExcStackFrame	esf;
	}	ExcInfo, *ExcInfoPtr;

extern Regs	ExcRegs;

#ifdef THINK_C
  #pragma parameter d0 GetStackPtr	()
  #pragma parameter d0 GetVBR	()
  #pragma parameter SetVBR (d0)
  #pragma parameter d0 GetStatusReg	()
  #pragma parameter SetStatusReg	(d0)
  #pragma parameter d0 GetInterruptMask	()
  #pragma parameter SetInterruptMask	(d0)
  #pragma parameter d0 GetFramePtr	()
#endif	/* THINK_C */
#ifdef AZTEC_C
  #pragma regcall (d0 GetStackPtr ())
  #pragma regcall (d0 GetVBR ())
  #pragma regcall (SetVBR (d0))
  #pragma regcall (d0 GetStatusReg ())
  #pragma regcall (SetStatusReg (d0))
  #pragma regcall (d0 GetInterruptMask ())
  #pragma regcall (SetInterruptMask (d0))
  #pragma regcall (d0 GetFramePtr ())
#endif	/* AZTEC_C */

#if defined(__MWERKS__)
  #pragma parameter __d0 GetStackPtr	()
void *GetStackPtr(void) = 
	{ 0x200F	/* MOVE A7,D0 */ };
  #pragma parameter __d0 GetVBR	()
void *GetVBR(void) =
	{ 0x4E7A, 0x0801 /* movec	vbr,d0 */ };
  #pragma parameter SetVBR (__d0)
void SetVBR(void *vb) =
	{ 0x4E7B, 0x0801 /* movec	d0,vbr */ };
  #pragma parameter __d0 GetStatusReg	()
ushort GetStatusReg(void) = 
	{ 0x40C0	/* MOVE SR,D0 */ };
  #pragma parameter SetStatusReg	(__d0)
void SetStatusReg(ushort mask) = 
	{ 0x46C0	/* MOVE D0,SR */ };
  #pragma parameter __d0 GetInterruptMask	()
ushort GetInterruptMask(void) = 
	{ 0x40C0, 0xE048, 0x0240, 0x0007 /* MOVE SR,D0 : LSR.W #$8,D0 : ANDI.W #$7,D0 */ };
  #pragma parameter SetInterruptMask	(__d0)
void SetInterruptMask(ushort mask) = 
	{ 0xE148, 0x0040, 0x2000, 0x46C0 /* LSL.W #$8,D0 : ORI.W #$2000,D0 : MOVE D0,SR */ };
  #pragma parameter __d0 GetFramePtr	()
ExcStackFrame *GetFramePtr(void) =
	{ 0x2008 /* move.l a0,d0 */ };
#elif defined(__GNUC__)

#define GetStackPtr()\
    ({void *__sp;\
      __asm__("movel %%sp,%0" : "=g" (__sp));\
      __sp;})
#define GetStatusReg()\
    ({ushort __sr;\
      __asm__("move %%sr,%0" : "=g" (__sr));\
      __sr;})
#define SetStatusReg(mask)\
    ({ushort __m = (mask);\
      __asm__("move %0,%%sr" :: "g" (__m));})

#define GetInterruptMask()\
    ({ushort __i;\
      __asm__("move %%sr,%0; lsrw #8,%0; andiw #7,%0"\
               : "=d" (__i));\
      __i;})
#define SetInterruptMask(i)\
    ({ushort __i = (i);\
      __asm__("lslw #8,%0; oriw #0x2000,%0; move %0,%%sr"\
               : : "d" (__i));})
#define GetVBR()\
    ({ulong __v;\
      __asm__("movec %%vbr,%0" : "=g" (__v));\
      __v;})
#define SetVBR(v)\
    ({ulong __v = (v);\
      __asm__("movec %0,%%vbr" : : "g" (__v));})

#define GetFramePtr()\
    ({ExcStackFrame *__fp;\
      __asm__("movel %%a0,%0" : "=g" (__fp));\
      __fp;})

#else	/* __MWERKS__ */

void *GetStackPtr(void) = 
	{ 0x200F	/* MOVE A7,D0 */ };
ushort GetStatusReg(void) = 
	{ 0x40C0	/* MOVE SR,D0 */ };
void SetStatusReg(ushort mask) = 
	{ 0x46C0	/* MOVE D0,SR */ };
ushort GetInterruptMask(void) = 
	{ 0x40C0, 0xE048, 0x0240, 0x0007 /* MOVE SR,D0 : LSR.W #$8,D0 : ANDI.W #$7,D0 */ };
void SetInterruptMask(ushort mask) = 
	{ 0xE148, 0x0040, 0x2000, 0x46C0 /* LSL.W #$8,D0 : ORI.W #$2000,D0 : MOVE D0,SR */ };
void *GetVBR(void) =
	{ 0x4E7A, 0x0801 /* movec	vbr,d0 */ };
void SetVBR(void *vb) =
	{ 0x4E7B, 0x0801 /* movec	d0,vbr */ };
ExcStackFrame *GetFramePtr(void) =
	{ 0x2008 /* move.l a0,d0 */ };
#endif	/* __MWERKS__ */


vfptr InstallHandler(vfptr functptr, short vector, ExcCFramePtr frame);
void InstallDefaultHandler(vfptr newHandler);	/* 3.05, 97/03/04--jhg */
                     
#define	NO_RUPTS_MASK		7
#define	ALL_RUPTS_MASK	0

void PicIRQHoldoffs(short enable);		/* 3.12, 97/11/21--jhg */


/*
**	FLASH (EEPROM)
*/
typedef enum
	{
	flashOk = 0,
	flashOddAddress,
	flashCantProgram
	} FlashErr;

FlashErr FlashID(ushort *startAddr, ushort *mfr, ushort *device);
FlashErr FlashBurnBlock(ushort *srcAddr, ushort *flashAddr, long byteCount);
FlashErr FlashError(ushort **errorAddr);


/*
**	POWER
*/
enum { v5, v3 };
void Stop(ushort sr);
void StopLP(ushort sr);
void VRegSelect(short vreg);	/* DO NOT USE THIS!  3.10, 97/07/30--jhg */
short VRegSwitchTo(short vreg, ushort loopdelay, ushort ruptmask);	/* 3.10 --jhg */

/*
**	OFFLOAD
*/
typedef enum
	{
	xmdmOk = 0,
	xmdmInitNAKtmt,			/* timeout waiting for initial NAK */
	xmdmRcvrCancelled,		/* receiver cancelled (sent CAN) */
	xmdmTooManyErrors,		/* too many errors (bad blocks) */
	xmdmWaitACKtmt,			/* timeout waiting for ACK */
	xmdmFinalACKtmt			/* timeout waiting for final ACK */
	} XmdmErr;

XmdmErr XmodemSendMem(void *address, long length, ushort timeout);


/*
**	RTC
*/
void SetTimeSecs(time_t secs, vfptr sync);
time_t RtcToCtm(void);
void SetAlarmSecs(time_t secs);
time_t AlarmToCtm(void);
#define SetTimeTM(tp,sync)	SetTimeSecs(mktime(tp), sync)
#define SetAlarmTM(tp)	SetAlarmSecs(mktime(tp))

/*
**	SCI (SERIAL CONTROLLER INTERFACE)
*/
void SerInFlush(void);
void SerSetInBuf(ptr buffer, long bufsize);
short SerGetByte(void);
void SerPutByte(ushort theByte);
short SerByteAvail(void);
short SerTimedGetByte(long msTimeout);
long SerGetBaud(long baud, long freq);
long SerSetBaud(long baud, long freq);
void SerShutDown(short immed, short autowake);
void SerActivate(void);

/* 96/11/15--jhg */
void SerSetFilterFunct(int (*filterFunct)(short status, uchar *data));
/* Install a buffered input filter C function. It will receive in
** status, the latest SCSR reading, and data points to the character
** just read from SCDR (which you can replace with an alternate). 
** You return non-zero to tell the buffer handler not to insert this
** in the buffer, or zero to install the passed or alternate value.
*/

/*
**	TIMING
*/
/*
**	This inline function enters CPU-32 loop mode executing nops for the short
**	count passed (32767 MAX!).  In loop mode, no instruction fetches are made
**	so this is independent of wait states.  Each iteration takes 8 cycles, so:
**	16MHz = 500nS/iter, 8 MHz = 1uS/iter, ... 160 kHz = 50us/iter
*/
#ifdef THINK_C
  #pragma parameter LMDelay	(d0)
void LMDelay (short count) = 
	{ 0x4E71, 0x51C8, 0xFFFC	/* loop: nop dbra d0,loop */ };
#endif	/* THINK_C */
#ifdef AZTEC_C
  #pragma regcall (LMDelay(d0))
void LMDelay (short count) = 
	{ 0x4E71, 0x51C8, 0xFFFC	/* loop: nop dbra d0,loop */ };
#endif	/* AZTEC_C */
#ifdef __MWERKS__
  #pragma parameter LMDelay	(__d0)
void LMDelay (short count) = 
	{ 0x4E71, 0x51C8, 0xFFFC	/* loop: nop dbra d0,loop */ };
#endif	/* __MWERKS__ */

#ifdef __GNUC__
#define LMDelay(c)\
    ({short __c = (c);\
      __asm__("0: nop; dbra %0,0b" : : "d" (__c) : "cc");})
#endif

typedef struct
	{
	time_t	secs;
	long	ticks;
	}	time_tt;

/*
**	MACRO TO SIMPLIFY COMPARISONS IN IF STATEMENTS
**		assume operator in place of comma (TTMLT(t1,t2) --> t1 LT t2)
*/
#define	TTMEQ(t1,t2)		(ttmcmp(t1,t2) == 0)
#define	TTMNE(t1,t2)		(ttmcmp(t1,t2) != 0)
#define	TTMLT(t1,t2)		(ttmcmp(t1,t2) < 0)
#define	TTMLE(t1,t2)		(ttmcmp(t1,t2) <= 0)
#define	TTMGT(t1,t2)		(ttmcmp(t1,t2) > 0)
#define	TTMGE(t1,t2)		(ttmcmp(t1,t2) >= 0)

long SimSetFSys(long freq);
long SimGetFSys(void);

ulong TensMilliSecs(void);
ulong MilliSecs(void);
void DelayMilliSecs(ulong mS);

void StopWatchStart(void);
ulong StopWatchTime(void);

#ifdef __GNUC__

/*
** Aztec C returns all structures (regardless of size) as pointers to a 
** static location.  It does this in a non-standard way which even the
** -fpcc-struct-return option to gcc cannot properly handle.
*/
time_tt *ttmadd(time_tt addend1, time_tt addend2);
time_tt *ttmnow(void);

#define ttm_now()\
    ({time_tt *__t = ttmnow();\
      *__t;})
#define ttm_add(a, b)\
    ({time_tt *__t = ttmadd((a), (b));\
      *__t;})

#else
time_tt ttmadd(time_tt addend1, time_tt addend2);
time_tt ttmnow(void);
#endif

long ttmcmp(time_tt t1, time_tt t2);
short Sleep(long ticks);
short SleepTill(time_tt wakeup);
short SetTickRate(long tickRate);
long GetTickRate(void);

/*
**	SERIAL EEPROM
*/

#define	ONSET_SEE_BYTES	256	/* Onset PIC software needs the first page */
							/* size and address offsets taken care of lib */
typedef enum
	{
	ueeOk = 0,
	ueeCantAccess,			/* timeout occured trying to access */
	ueeCantProgram,			/* write operation didn't verify */
	ueeAddrError			/* invalid address or range specified */
	} UeeErr;
ushort UeeSize(void);
UeeErr UeeError(ushort *errorLocation);
UeeErr UeeReadByte(ushort address, uchar *data);
UeeErr UeeReadBlock(ushort ueeSrcAddr, uchar *buffer, short len);
UeeErr UeeCalcCRC(ushort address, short len, ushort *crc);
UeeErr UeeTestBit(ushort address, ushort bitno, short *ishigh);
UeeErr UeeErase(void);
UeeErr UeeFill(ushort data);
UeeErr UeeWriteByte(ushort address, ushort data);
UeeErr UeeWriteBlock(ushort ueeDestAddr, uchar *buffer, short len);
UeeErr UeeSetBit(ushort address, ushort bitno);
UeeErr UeeClearBit(ushort address, ushort bitno);

/*
**	TIME PROCESSOR UNIT
*/
#ifdef THINK_C
  #pragma parameter TPUInterruptEnable (d0)
void TPUInterruptEnable(short chan) =
	{ 0x7201, 0xE169, 0x8378, 0xFE0A /* MOVEQ #$01,D1 : LSL.W D0,D1 : OR.W D1,CIER */ };
  #pragma parameter TPUInterruptDisable (d0)
void TPUInterruptDisable(short chan) =
	{ 0x7201, 0xE169, 0x4641, 0xC378, 0xFE0A
	/* MOVEQ #$01,D1 : LSL.W D0,D1 : NOT.W D1 : AND.W D1,CIER */ };
#endif	/* THINK_C */
#ifdef AZTEC_C
  #pragma regcall (TPUInterruptEnable (d0))
void TPUInterruptEnable(short chan) =
	{ 0x7201, 0xE169, 0x8378, 0xFE0A /* MOVEQ #$01,D1 : LSL.W D0,D1 : OR.W D1,CIER */ };
  #pragma regcall (TPUInterruptDisable (d0))
void TPUInterruptDisable(short chan) =
	{ 0x7201, 0xE169, 0x4641, 0xC378, 0xFE0A
	/* MOVEQ #$01,D1 : LSL.W D0,D1 : NOT.W D1 : AND.W D1,CIER */ };
#endif	/* AZTEC_C */
#ifdef __MWERKS__
  #pragma parameter TPUInterruptEnable (__d0)
void TPUInterruptEnable(short chan) =
	{ 0x7201, 0xE169, 0x8378, 0xFE0A /* MOVEQ #$01,D1 : LSL.W D0,D1 : OR.W D1,CIER */ };
  #pragma parameter TPUInterruptDisable (__d0)
void TPUInterruptDisable(short chan) =
	{ 0x7201, 0xE169, 0x4641, 0xC378, 0xFE0A
	/* MOVEQ #$01,D1 : LSL.W D0,D1 : NOT.W D1 : AND.W D1,CIER */ };
#endif	/* __MWERKS__ */

#ifdef __GNUC__
#define TPUInterruptEnable(chan)\
    ({short __c = (chan); uspv __cier = CIER;\
      __asm__ volatile ("moveq #1,%%d1;\
                         lslw %0,%%d1;\
                         orw %%d1,(%1)"\
                         : : "d" (__c), "a" (__cier) : "%d1");})
#define TPUInterruptDisable(chan)\
    ({short __c = (chan); uspv __cier = CIER;\
      __asm__ volatile ("moveq #1,%%d1;\
                         lslw %0,%%d1;\
                         notw %%d1;\
                         andw %%d1,(%1)"\
                         : : "d" (__c), "a" (__cier) : "%d1");})
#endif

#define	TPUGetInterrupt(chan)		(*CISR & (1 << chan))
#define	TPUClearInterrupt(chan)	(*CISR &= (~(1 << chan)))


void TPUSetPin(short chan, short pinval);
int TPUGetPin(short chan);
ulong TPUGetTCR1(void);

/*
**	TPU SERIAL UART
*/
enum {
	tsOK = 0,
	tsAlreadyOpen,		/* cant open because it is already open */
	tsInvalidQSize,		/* queue size request must be even binary multiple */
	tsInternalError,	/* shouldn't happen, contact Onset */
	tsZeroBaudErr		/* 97/01/25--jhg, added to prevent /zero excecptions */
	};

#define	TSER_MIN_MEM	64	/* buffer must allocate this + qsize */

		/*********************************************
		**	F U N C T I O N   P R O T O T Y P E S	**
		*********************************************/
int								/* error code, zero = success */
	TSerOpen(					/* open TPU channel for UART I/O */
		int		chan,			/* TPU channel, 0..15 */
		int		priority,		/* TPU: LowPrior, MiddlePrior, HighPrior */
		int		outflag,		/* zero = input, non-zero = output */
		ptr		buffer,			/* pointer to queue buffer  */ 
		long	qsize,			/* buffer size (2^n 64, 128, ... 4096) */ 
		long	baud, 			/* requested baud rate */
		int		parity,			/* 'E' = even, 'O' = odd, else none */
		int		databits,		/* 0 = 8, anything else is used directly */
		int		stopbits);		/* 0 = 1, 1 = 1, 2 = 2, ... */

int								/* error code, zero = success */
	TSerClose(					/* close an opened UART TPU channel */
		int		chan);			/* TPU channel, 0..15 */

long							/* actual settable rate */
	TSerResetBaud(				/* setup new baud rate for UART channel */
		int		chan,			/* TPU channel, 0..15 */
		long	baud); 			/* requested baud rate */

int								/* non-zero if one or more bytes available */
	TSerByteAvail(				/* test for data available from input UART */
		int		chan);			/* TPU channel, 0..15 */

int								/* next byte from input queue */
	TSerGetByte(				/* wait for and return next input byte */
		int		chan);			/* TPU channel, 0..15 */

void
	TSerInFlush(				/* flush UART input queue */
		int		chan);			/* TPU channel, 0..15 */

void
	TSerPutByte(				/* write byte to UART output queue */
		int		chan,			/* TPU channel, 0..15 */
		int		data);			/* byte to write */

#define	TSerOutQEmpty	TSerByteAvail	/* DON'T USE: here for backward compatibility */
#define	TSerOutQBytes	TSerByteAvail	/* return number of bytes in output queue */

#define	TT8TUIN			14
#define	TT8TUOUT		13

/* 96/11/15--jhg */
#define	TSerInQBytes	TSerByteAvail	/* return number of bytes in input queue */
uchar *							/* pointer to the start of the queue data buffer */
	TSerGetQueue(				/* return low level queue information */
		int		chan,			/* TPU channel, 0..15 */
		int		*head,			/* index into queue for first character */
		int		*tail,			/* index into queue for last character -1 */
		int		*size);			/* size of queue for wrap manipulation */

/*
**	MISCELLANEOUS
*/
#ifdef __GNUC__
#define Reset()\
    ({uchar *__pic = (uchar*)0x00e00000;\
      __asm__ volatile ("moveb #0xc0,(%0); reset" : : "a" (__pic));})

#define ResetToMon()\
    ({uchar *__pic = (uchar*)0x00e00000;\
      __asm__ volatile ("moveb #0xc1,(%0); reset" : : "a" (__pic));})
#else

void Reset(void) =
	{ 0x13FC, 0x00C0, 0x00E0, 0x0000, 0x4E70 /* MOVE.B #$C0,$00E00000 : RESET */ };
void ResetToMon(void) =
	{ 0x13FC, 0x00C1, 0x00E0, 0x0000, 0x4E70 /* MOVE.B #$C1,$00E00000 : RESET */ };
#endif

ptr NumToHexStr(ulong num, ptr str, short digits);
ptr InitRam(short waits);
ptr GetRamInfo(long *size, short *waits);
short SimSetFlashWaits(short waits);
short SimSetRAMWaits(short waits);
short InputLine(ptr linebuf, short linelen);
void PutStr(char *str);
ptr GetFlashInfo(long *size, short *waits);		/* 95/08/22 --jhg */

extern char *__TT8LIB_DATE__;

#define NO_WATCHDOG		-1	/* for InitTT8() & InitSIM() calls */
#define TT8_TPU			-1	/* for InitTT8() & InitTPU() calls */
enum {
	noDebugger = 0,	/* default, no special initialization accomodations */
	rdb68sci,				/* remote debugger via QSM serial controller (#1) */
	rdb68tpu,				/* remote debugger via TPU serial 13 & 14 (#2) */
	otherDebugger		/* don't initialize VBR */
	};
extern short	__Debugger__;
void InitTT8(short watchdog, short tpumcr);
void InitSIM(short watchdog);
void InitQSM(void);
void InitVBR(void *vb);
void InitTPU(short tpumcr);
void InitPorts(void);

typedef struct
	{
	long		libVersion;		/* v/0x100.v%0x100, none prior to 2.00 95/08/22 */
	ptr			ramBase;		/* base address of RAM */
	long		ramSize;		/* size of RAM in bytes */
	short		ramWaits;		/* RAM wait states */
	ptr			flashBase;		/* base address of Flash */
	long		flashSize;		/* size of Flash in bytes */
	short		flashWaits;		/* Flash wait states */
	void		*heapBot;		/* heap bottom (don't mess with this) */
	void		*heapTop;		/* heap top (or this) */
	void		*heapCur;		/* current heap pointer (this either!) */
	ushort		statusReg;		/* current status register */
	void		*vectorBase;	/* current vector base address */
	void		*stackPtr;		/* current stack pointer */
	long		clibVersion;	/* v/0x100.v%0x100, (both placeholders, init. zero) */
	long		mlibVersion;	/* v/0x100.v%0x100, none prior to 3.00 96/09/29 */
	char		*libBuildDate;	/* string represenation of build date for tt8.lib */
	char		*clibBuildDate;	/* as above for cl16tt8.lib (placeholder, init. NULL) */
	char		*mlibBuildDate;	/* as above for cl16tt8.lib (placeholder, init. NULL) */
	} TT8info;

typedef enum { returnStruct, shortDisplay, fullDisplay } infoAction;
TT8info *InfoTT8(infoAction action);

#ifdef tt8_time_t
  #undef	time_t
#endif

#endif	/*	__tt8lib_H */

