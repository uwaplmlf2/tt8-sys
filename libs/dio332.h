/*******************************************************************************
**	dio332.h -- Tattletale 68332 Digital I/O Port Pin Definitions
**
**          Copyright 1994 ONSET Computer Corp.  All rights reserved.
**	
**	Friday, April 1, 1994	9:05 AM	  --jhg
*******************************************************************************/

#ifndef		__dio332_H
#define		__dio332_H

#include	<sim332.h>

#ifdef __GNUC__
#define PSet(port, pin)		PSet ##port(pin)
#define PClear(port, pin)	PClear ##port(pin)
#define PChange(port, pin)	PChange ##port(pin)
#define	Pin(port,pin)		Pin ##port(pin)
#define	PConfOutp(port,pin)	PConfOutp ##port(pin)
#define	PConfInp(port,pin)	PConfInp ##port(pin)
#define	PConfBus(port,pin)	PConfBus ##port(pin)
#define	PInpToOut(port,pin)	PInpToOut ##port(pin)
#else
#define		PSet(port,pin)		PSet ##port ##pin()
#define		PClear(port,pin)	PClear ##port ##pin()
#define		PChange(port,pin)	PChange ##port ##pin()
#define		Pin(port,pin)		Pin ##port ##pin()
#define		PConfOutp(port,pin)	PConfOutp ##port ##pin()
#define		PConfInp(port,pin)	PConfInp ##port ##pin()
#define		PConfBus(port,pin)	PConfBus ##port ##pin()
#define		PInpToOut(port,pin)	PInpToOut ##port ##pin()
#endif

#define	BSETBI	0x08F8
#define	BCLRBI	0x08B8
#define	BCHGBI	0x0878
#define	BTSTBI	0x0838
#define	SNENEGEXTWD0	0x56C0,0x4400,0x4880
#define	MOVEBII	0x11F8

#ifdef __GNUC__
#define PinSet(port, pin)\
    ({ucpv __port = (port); short __pin = (pin);\
      __asm__("bset %0,%1@" : : "d" (__pin), "a" (__port));})
#define PinClear(port, pin)\
    ({ucpv __port = (port); short __pin = (pin);\
      __asm__("bclr %0,%1@" : : "d" (__pin), "a" (__port));})
#define PinChange(port, pin)\
    ({ucpv __port = (port); short __pin = (pin);\
      __asm__("bchg %0,%1@" : : "d" (__pin), "a" (__port));})
#define PinTest(port, pin)\
    ({ucpv __port = (port); short __pin = (pin); short __r;\
      __asm__("btst %1,%2@;\
               sne %0;\
               negb %0;\
               extw %0;"\
               : "=d" (__r) : "d" (__pin), "a" (__port)); __r;})
#define PinConfOutput(ioreg, dreg, pin)\
    ({ucpv __io = (ioreg); ucpv __dd = (dreg); short __pin = (pin);\
      __asm__("bclr %0,%1@;\
               bset %0,%2@;"\
               : : "d" (__pin), "a" (__io), "a" (__dd));})
#define PinConfInput(ioreg, dreg, pin)\
    ({ucpv __io = (ioreg); ucpv __dd = (dreg); short __pin = (pin);\
      __asm__("bclr %0,%1@;\
               bclr %0,%2@;"\
               : : "d" (__pin), "a" (__io), "a" (__dd));})
#define PinConfBus(ioreg, pin)\
    ({ucpv __io = (ioreg); short __pin = (pin);\
      __asm__("bset %0,%1@"\
               : : "d" (__pin), "a" (__io));})
#define PinConfInputToOutput(port, ioreg, dreg, pin)\
    ({ucpv __port = (port); ucpv __io = (ioreg); ucpv __dd = (dreg);\
      short __pin = (pin);\
      __asm__("bclr %0,%1@;\
               bclr %0,%2@;\
               moveb %3@,%3@;\
               bset %0,%2@;"\
               : : "d" (__pin), "a" (__io), "a" (__dd), "a" (__port));})
#endif /* __GNUC__ */

#ifdef __GNUC__

#define PSetD(pin)		PinSet(PORTD, pin)
#define PClearD(pin)		PinClear(PORTD, pin)
#define PChangeD(pin)		PinChange(PORTD, pin)
#define PinD(pin)		PinTest(PORTD, pin)
#define PConfOutpD(pin)		PinConfOutput(PDPAR, DDRD, pin)
#define PConfInpD(pin)		PinConfInput(PDPAR, DDRD, pin)
#define PConfBusD(pin)		PinConfBus(PDPAR, pin)
#define PInpToOutD(pin)		PinConfInputToOutput(PORTD, PDPAR, DDRD, pin)

#else
void PSetD0(void) = { BSETBI, 0, (ushort) PORTD };
	#define	PSetd0	PSetD0
void PClearD0(void) = { BCLRBI, 0, (ushort) PORTD };
	#define	PCleard0	PClearD0
void PChangeD0(void) = { BCHGBI, 0, (ushort) PORTD };
	#define	PChanged0	PChangeD0
int PinD0(void) = { BTSTBI, 0, (ushort) PORTD, SNENEGEXTWD0 };
	#define	Pind0	PinD0
void PConfOutpD0(void) = { BCLRBI, 0, (ushort) PDPAR, BSETBI, 0, (ushort) DDRD };
	#define	PConfOutpd0	PConfOutpD0
void PConfInpD0(void) = { BCLRBI, 0, (ushort) PDPAR, BCLRBI, 0, (ushort) DDRD };
	#define	PConfInpd0	PConfInpD0
void PConfBusD0(void) = { BSETBI, 0, (ushort) PDPAR };
	#define	PConfBusd0	PConfBusD0
void PInpToOutD0(void) = { BCLRBI, 0, (ushort) PDPAR, BCLRBI, 0, (ushort) DDRD, 
	MOVEBII, (ushort)PORTD&0xFFFF, (ushort)PORTD&0xFFFF, BSETBI, 0, (ushort) DDRD };
	#define	PInpToOutd0	PInpToOutD0

void PSetD1(void) = { BSETBI, 1, (ushort) PORTD };
	#define	PSetd1	PSetD1
void PClearD1(void) = { BCLRBI, 1, (ushort) PORTD };
	#define	PCleard1	PClearD1
void PChangeD1(void) = { BCHGBI, 1, (ushort) PORTD };
	#define	PChanged1	PChangeD1
int PinD1(void) = { BTSTBI, 1, (ushort) PORTD, SNENEGEXTWD0 };
	#define	Pind1	PinD1
void PConfOutpD1(void) = { BCLRBI, 1, (ushort) PDPAR, BSETBI, 1, (ushort) DDRD };
	#define	PConfOutpd1	PConfOutpD1
void PConfInpD1(void) = { BCLRBI, 1, (ushort) PDPAR, BCLRBI, 1, (ushort) DDRD };
	#define	PConfInpd1	PConfInpD1
void PConfBusD1(void) = { BSETBI, 1, (ushort) PDPAR };
	#define	PConfBusd1	PConfBusD1
void PInpToOutD1(void) = { BCLRBI, 1, (ushort) PDPAR, BCLRBI, 1, (ushort) DDRD, 
	MOVEBII, (ushort)PORTD&0xFFFF, (ushort)PORTD&0xFFFF, BSETBI, 1, (ushort) DDRD };
	#define	PInpToOutd1	PInpToOutD1

void PSetD2(void) = { BSETBI, 2, (ushort) PORTD };
	#define	PSetd2	PSetD2
void PClearD2(void) = { BCLRBI, 2, (ushort) PORTD };
	#define	PCleard2	PClearD2
void PChangeD2(void) = { BCHGBI, 2, (ushort) PORTD };
	#define	PChanged2	PChangeD2
int PinD2(void) = { BTSTBI, 2, (ushort) PORTD, SNENEGEXTWD0 };
	#define	Pind2	PinD2
void PConfOutpD2(void) = { BCLRBI, 2, (ushort) PDPAR, BSETBI, 2, (ushort) DDRD };
	#define	PConfOutpd2	PConfOutpD2
void PConfInpD2(void) = { BCLRBI, 2, (ushort) PDPAR, BCLRBI, 2, (ushort) DDRD };
	#define	PConfInpd2	PConfInpD2
void PConfBusD2(void) = { BSETBI, 2, (ushort) PDPAR };
	#define	PConfBusd2	PConfBusD2
void PInpToOutD2(void) = { BCLRBI, 2, (ushort) PDPAR, BCLRBI, 2, (ushort) DDRD, 
	MOVEBII, (ushort)PORTD&0xFFFF, (ushort)PORTD&0xFFFF, BSETBI, 2, (ushort) DDRD };
	#define	PInpToOutd2	PInpToOutD2

void PSetD3(void) = { BSETBI, 3, (ushort) PORTD };
	#define	PSetd3	PSetD3
void PClearD3(void) = { BCLRBI, 3, (ushort) PORTD };
	#define	PCleard3	PClearD3
void PChangeD3(void) = { BCHGBI, 3, (ushort) PORTD };
	#define	PChanged3	PChangeD3
int PinD3(void) = { BTSTBI, 3, (ushort) PORTD, SNENEGEXTWD0 };
	#define	Pind3	PinD3
void PConfOutpD3(void) = { BCLRBI, 3, (ushort) PDPAR, BSETBI, 3, (ushort) DDRD };
	#define	PConfOutpd3	PConfOutpD3
void PConfInpD3(void) = { BCLRBI, 3, (ushort) PDPAR, BCLRBI, 3, (ushort) DDRD };
	#define	PConfInpd3	PConfInpD3
void PConfBusD3(void) = { BSETBI, 3, (ushort) PDPAR };
	#define	PConfBusd3	PConfBusD3
void PInpToOutD3(void) = { BCLRBI, 3, (ushort) PDPAR, BCLRBI, 3, (ushort) DDRD, 
	MOVEBII, (ushort)PORTD&0xFFFF, (ushort)PORTD&0xFFFF, BSETBI, 3, (ushort) DDRD };
	#define	PInpToOutd3	PInpToOutD3

void PSetD4(void) = { BSETBI, 4, (ushort) PORTD };
	#define	PSetd4	PSetD4
void PClearD4(void) = { BCLRBI, 4, (ushort) PORTD };
	#define	PCleard4	PClearD4
void PChangeD4(void) = { BCHGBI, 4, (ushort) PORTD };
	#define	PChanged4	PChangeD4
int PinD4(void) = { BTSTBI, 4, (ushort) PORTD, SNENEGEXTWD0 };
	#define	Pind4	PinD4
void PConfOutpD4(void) = { BCLRBI, 4, (ushort) PDPAR, BSETBI, 4, (ushort) DDRD };
	#define	PConfOutpd4	PConfOutpD4
void PConfInpD4(void) = { BCLRBI, 4, (ushort) PDPAR, BCLRBI, 4, (ushort) DDRD };
	#define	PConfInpd4	PConfInpD4
void PConfBusD4(void) = { BSETBI, 4, (ushort) PDPAR };
	#define	PConfBusd4	PConfBusD4
void PInpToOutD4(void) = { BCLRBI, 4, (ushort) PDPAR, BCLRBI, 4, (ushort) DDRD, 
	MOVEBII, (ushort)PORTD&0xFFFF, (ushort)PORTD&0xFFFF, BSETBI, 4, (ushort) DDRD };
	#define	PInpToOutd4	PInpToOutD4

void PSetD5(void) = { BSETBI, 5, (ushort) PORTD };
	#define	PSetd5	PSetD5
void PClearD5(void) = { BCLRBI, 5, (ushort) PORTD };
	#define	PCleard5	PClearD5
void PChangeD5(void) = { BCHGBI, 5, (ushort) PORTD };
	#define	PChanged5	PChangeD5
int PinD5(void) = { BTSTBI, 5, (ushort) PORTD, SNENEGEXTWD0 };
	#define	Pind5	PinD5
void PConfOutpD5(void) = { BCLRBI, 5, (ushort) PDPAR, BSETBI, 5, (ushort) DDRD };
	#define	PConfOutpd5	PConfOutpD5
void PConfInpD5(void) = { BCLRBI, 5, (ushort) PDPAR, BCLRBI, 5, (ushort) DDRD };
	#define	PConfInpd5	PConfInpD5
void PConfBusD5(void) = { BSETBI, 5, (ushort) PDPAR };
	#define	PConfBusd5	PConfBusD5
void PInpToOutD5(void) = { BCLRBI, 5, (ushort) PDPAR, BCLRBI, 5, (ushort) DDRD, 
	MOVEBII, (ushort)PORTD&0xFFFF, (ushort)PORTD&0xFFFF, BSETBI, 5, (ushort) DDRD };
	#define	PInpToOutd5	PInpToOutD5

void PSetD6(void) = { BSETBI, 6, (ushort) PORTD };
	#define	PSetd6	PSetD6
void PClearD6(void) = { BCLRBI, 6, (ushort) PORTD };
	#define	PCleard6	PClearD6
void PChangeD6(void) = { BCHGBI, 6, (ushort) PORTD };
	#define	PChanged6	PChangeD6
int PinD6(void) = { BTSTBI, 6, (ushort) PORTD, SNENEGEXTWD0 };
	#define	Pind6	PinD6
void PConfOutpD6(void) = { BCLRBI, 6, (ushort) PDPAR, BSETBI, 6, (ushort) DDRD };
	#define	PConfOutpd6	PConfOutpD6
void PConfInpD6(void) = { BCLRBI, 6, (ushort) PDPAR, BCLRBI, 6, (ushort) DDRD };
	#define	PConfInpd6	PConfInpD6
void PConfBusD6(void) = { BSETBI, 6, (ushort) PDPAR };
	#define	PConfBusd6	PConfBusD6
void PInpToOutD6(void) = { BCLRBI, 6, (ushort) PDPAR, BCLRBI, 6, (ushort) DDRD, 
	MOVEBII, (ushort)PORTD&0xFFFF, (ushort)PORTD&0xFFFF, BSETBI, 6, (ushort) DDRD };
	#define	PInpToOutd6	PInpToOutD6

void PSetD7(void) = { BSETBI, 7, (ushort) PORTD };
	#define	PSetd7	PSetD7
void PClearD7(void) = { BCLRBI, 7, (ushort) PORTD };
	#define	PCleard7	PClearD7
void PChangeD7(void) = { BCHGBI, 7, (ushort) PORTD };
	#define	PChanged7	PChangeD7
int PinD7(void) = { BTSTBI, 7, (ushort) PORTD, SNENEGEXTWD0 };
	#define	Pind7	PinD7
void PConfOutpD7(void) = { BCLRBI, 7, (ushort) PDPAR, BSETBI, 7, (ushort) DDRD };
	#define	PConfOutpd7	PConfOutpD7
void PConfInpD7(void) = { BCLRBI, 7, (ushort) PDPAR, BCLRBI, 7, (ushort) DDRD };
	#define	PConfInpd7	PConfInpD7
void PConfBusD7(void) = { BSETBI, 7, (ushort) PDPAR };
	#define	PConfBusd7	PConfBusD7
void PInpToOutD7(void) = { BCLRBI, 7, (ushort) PDPAR, BCLRBI, 7, (ushort) DDRD, 
	MOVEBII, (ushort)PORTD&0xFFFF, (ushort)PORTD&0xFFFF, BSETBI, 7, (ushort) DDRD };
	#define	PInpToOutd7	PInpToOutD7
#endif /* __GNUC__ */


#ifdef __GNUC__

#define PSetE(pin)		PinSet(PORTE, pin)
#define PClearE(pin)		PinClear(PORTE, pin)
#define PChangeE(pin)		PinChange(PORTE, pin)
#define PinE(pin)		PinTest(PORTE, pin)
#define PConfOutpE(pin)		PinConfOutput(PEPAR, DDRE, pin)
#define PConfInpE(pin)		PinConfInput(PEPAR, DDRE, pin)
#define PConfBusE(pin)		PinConfBus(PEPAR, pin)
#define PInpToOutE(pin)		PinConfInputToOutput(PORTE, PEPAR, DDRE, pin)

#else

void PSetE0(void) = { BSETBI, 0, (ushort) PORTE };
	#define	PSete0	PSetE0
void PClearE0(void) = { BCLRBI, 0, (ushort) PORTE };
	#define	PCleare0	PClearE0
void PChangeE0(void) = { BCHGBI, 0, (ushort) PORTE };
	#define	PChangee0	PChangeE0
int PinE0(void) = { BTSTBI, 0, (ushort) PORTE, SNENEGEXTWD0 };
	#define	Pine0	PinE0
void PConfOutpE0(void) = { BCLRBI, 0, (ushort) PEPAR, BSETBI, 0, (ushort) DDRE };
	#define	PConfOutpe0	PConfOutpE0
void PConfInpE0(void) = { BCLRBI, 0, (ushort) PEPAR, BCLRBI, 0, (ushort) DDRE };
	#define	PConfInpe0	PConfInpE0
void PConfBusE0(void) = { BSETBI, 0, (ushort) PEPAR };
	#define	PConfBuse0	PConfBusE0
void PInpToOutE0(void) = { BCLRBI, 0, (ushort) PEPAR, BCLRBI, 0, (ushort) DDRE, 
	MOVEBII, (ushort)PORTE&0xFFFF, (ushort)PORTE&0xFFFF, BSETBI, 0, (ushort) DDRE };
	#define	PInpToOute0	PInpToOutE0

void PSetE1(void) = { BSETBI, 1, (ushort) PORTE };
	#define	PSete1	PSetE1
void PClearE1(void) = { BCLRBI, 1, (ushort) PORTE };
	#define	PCleare1	PClearE1
void PChangeE1(void) = { BCHGBI, 1, (ushort) PORTE };
	#define	PChangee1	PChangeE1
int PinE1(void) = { BTSTBI, 1, (ushort) PORTE, SNENEGEXTWD0 };
	#define	Pine1	PinE1
void PConfOutpE1(void) = { BCLRBI, 1, (ushort) PEPAR, BSETBI, 1, (ushort) DDRE };
	#define	PConfOutpe1	PConfOutpE1
void PConfInpE1(void) = { BCLRBI, 1, (ushort) PEPAR, BCLRBI, 1, (ushort) DDRE };
	#define	PConfInpe1	PConfInpE1
void PConfBusE1(void) = { BSETBI, 1, (ushort) PEPAR };
	#define	PConfBuse1	PConfBusE1
void PInpToOutE1(void) = { BCLRBI, 1, (ushort) PEPAR, BCLRBI, 1, (ushort) DDRE, 
	MOVEBII, (ushort)PORTE&0xFFFF, (ushort)PORTE&0xFFFF, BSETBI, 1, (ushort) DDRE };
	#define	PInpToOute1	PInpToOutE1

void PSetE2(void) = { BSETBI, 2, (ushort) PORTE };
	#define	PSete2	PSetE2
void PClearE2(void) = { BCLRBI, 2, (ushort) PORTE };
	#define	PCleare2	PClearE2
void PChangeE2(void) = { BCHGBI, 2, (ushort) PORTE };
	#define	PChangee2	PChangeE2
int PinE2(void) = { BTSTBI, 2, (ushort) PORTE, SNENEGEXTWD0 };
	#define	Pine2	PinE2
void PConfOutpE2(void) = { BCLRBI, 2, (ushort) PEPAR, BSETBI, 2, (ushort) DDRE };
	#define	PConfOutpe2	PConfOutpE2
void PConfInpE2(void) = { BCLRBI, 2, (ushort) PEPAR, BCLRBI, 2, (ushort) DDRE };
	#define	PConfInpe2	PConfInpE2
void PConfBusE2(void) = { BSETBI, 2, (ushort) PEPAR };
	#define	PConfBuse2	PConfBusE2
void PInpToOutE2(void) = { BCLRBI, 2, (ushort) PEPAR, BCLRBI, 2, (ushort) DDRE, 
	MOVEBII, (ushort)PORTE&0xFFFF, (ushort)PORTE&0xFFFF, BSETBI, 2, (ushort) DDRE };
	#define	PInpToOute2	PInpToOutE2

void PSetE3(void) = { BSETBI, 3, (ushort) PORTE };
	#define	PSete3	PSetE3
void PClearE3(void) = { BCLRBI, 3, (ushort) PORTE };
	#define	PCleare3	PClearE3
void PChangeE3(void) = { BCHGBI, 3, (ushort) PORTE };
	#define	PChangee3	PChangeE3
int PinE3(void) = { BTSTBI, 3, (ushort) PORTE, SNENEGEXTWD0 };
	#define	Pine3	PinE3
void PConfOutpE3(void) = { BCLRBI, 3, (ushort) PEPAR, BSETBI, 3, (ushort) DDRE };
	#define	PConfOutpe3	PConfOutpE3
void PConfInpE3(void) = { BCLRBI, 3, (ushort) PEPAR, BCLRBI, 3, (ushort) DDRE };
	#define	PConfInpe3	PConfInpE3
void PConfBusE3(void) = { BSETBI, 3, (ushort) PEPAR };
	#define	PConfBuse3	PConfBusE3
void PInpToOutE3(void) = { BCLRBI, 3, (ushort) PEPAR, BCLRBI, 3, (ushort) DDRE, 
	MOVEBII, (ushort)PORTE&0xFFFF, (ushort)PORTE&0xFFFF, BSETBI, 3, (ushort) DDRE };
	#define	PInpToOute3	PInpToOutE3

void PSetE4(void) = { BSETBI, 4, (ushort) PORTE };
	#define	PSete4	PSetE4
void PClearE4(void) = { BCLRBI, 4, (ushort) PORTE };
	#define	PCleare4	PClearE4
void PChangeE4(void) = { BCHGBI, 4, (ushort) PORTE };
	#define	PChangee4	PChangeE4
int PinE4(void) = { BTSTBI, 4, (ushort) PORTE, SNENEGEXTWD0 };
	#define	Pine4	PinE4
void PConfOutpE4(void) = { BCLRBI, 4, (ushort) PEPAR, BSETBI, 4, (ushort) DDRE };
	#define	PConfOutpe4	PConfOutpE4
void PConfInpE4(void) = { BCLRBI, 4, (ushort) PEPAR, BCLRBI, 4, (ushort) DDRE };
	#define	PConfInpe4	PConfInpE4
void PConfBusE4(void) = { BSETBI, 4, (ushort) PEPAR };
	#define	PConfBuse4	PConfBusE4
void PInpToOutE4(void) = { BCLRBI, 4, (ushort) PEPAR, BCLRBI, 4, (ushort) DDRE, 
	MOVEBII, (ushort)PORTE&0xFFFF, (ushort)PORTE&0xFFFF, BSETBI, 4, (ushort) DDRE };
	#define	PInpToOute4	PInpToOutE4

void PSetE5(void) = { BSETBI, 5, (ushort) PORTE };
	#define	PSete5	PSetE5
void PClearE5(void) = { BCLRBI, 5, (ushort) PORTE };
	#define	PCleare5	PClearE5
void PChangeE5(void) = { BCHGBI, 5, (ushort) PORTE };
	#define	PChangee5	PChangeE5
int PinE5(void) = { BTSTBI, 5, (ushort) PORTE, SNENEGEXTWD0 };
	#define	Pine5	PinE5
void PConfOutpE5(void) = { BCLRBI, 5, (ushort) PEPAR, BSETBI, 5, (ushort) DDRE };
	#define	PConfOutpe5	PConfOutpE5
void PConfInpE5(void) = { BCLRBI, 5, (ushort) PEPAR, BCLRBI, 5, (ushort) DDRE };
	#define	PConfInpe5	PConfInpE5
void PConfBusE5(void) = { BSETBI, 5, (ushort) PEPAR };
	#define	PConfBuse5	PConfBusE5
void PInpToOutE5(void) = { BCLRBI, 5, (ushort) PEPAR, BCLRBI, 5, (ushort) DDRE, 
	MOVEBII, (ushort)PORTE&0xFFFF, (ushort)PORTE&0xFFFF, BSETBI, 5, (ushort) DDRE };
	#define	PInpToOute5	PInpToOutE5

void PSetE6(void) = { BSETBI, 6, (ushort) PORTE };
	#define	PSete6	PSetE6
void PClearE6(void) = { BCLRBI, 6, (ushort) PORTE };
	#define	PCleare6	PClearE6
void PChangeE6(void) = { BCHGBI, 6, (ushort) PORTE };
	#define	PChangee6	PChangeE6
int PinE6(void) = { BTSTBI, 6, (ushort) PORTE, SNENEGEXTWD0 };
	#define	Pine6	PinE6
void PConfOutpE6(void) = { BCLRBI, 6, (ushort) PEPAR, BSETBI, 6, (ushort) DDRE };
	#define	PConfOutpe6	PConfOutpE6
void PConfInpE6(void) = { BCLRBI, 6, (ushort) PEPAR, BCLRBI, 6, (ushort) DDRE };
	#define	PConfInpe6	PConfInpE6
void PConfBusE6(void) = { BSETBI, 6, (ushort) PEPAR };
	#define	PConfBuse6	PConfBusE6
void PInpToOutE6(void) = { BCLRBI, 6, (ushort) PEPAR, BCLRBI, 6, (ushort) DDRE, 
	MOVEBII, (ushort)PORTE&0xFFFF, (ushort)PORTE&0xFFFF, BSETBI, 6, (ushort) DDRE };
	#define	PInpToOute6	PInpToOutE6

void PSetE7(void) = { BSETBI, 7, (ushort) PORTE };
	#define	PSete7	PSetE7
void PClearE7(void) = { BCLRBI, 7, (ushort) PORTE };
	#define	PCleare7	PClearE7
void PChangeE7(void) = { BCHGBI, 7, (ushort) PORTE };
	#define	PChangee7	PChangeE7
int PinE7(void) = { BTSTBI, 7, (ushort) PORTE, SNENEGEXTWD0 };
	#define	Pine7	PinE7
void PConfOutpE7(void) = { BCLRBI, 7, (ushort) PEPAR, BSETBI, 7, (ushort) DDRE };
	#define	PConfOutpe7	PConfOutpE7
void PConfInpE7(void) = { BCLRBI, 7, (ushort) PEPAR, BCLRBI, 7, (ushort) DDRE };
	#define	PConfInpe7	PConfInpE7
void PConfBusE7(void) = { BSETBI, 7, (ushort) PEPAR };
	#define	PConfBuse7	PConfBusE7
void PInpToOutE7(void) = { BCLRBI, 7, (ushort) PEPAR, BCLRBI, 7, (ushort) DDRE, 
	MOVEBII, (ushort)PORTE&0xFFFF, (ushort)PORTE&0xFFFF, BSETBI, 7, (ushort) DDRE };
	#define	PInpToOute7	PInpToOutE7
#endif /* __GNUC__ */


#ifdef __GNUC__

#define PSetF(pin)		PinSet(PORTF, pin)
#define PClearF(pin)		PinClear(PORTF, pin)
#define PChangeF(pin)		PinChangF(PORTF, pin)
#define PinF(pin)		PinTest(PORTF, pin)
#define PConfOutpF(pin)		PinConfOutput(PFPAR, DDRF, pin)
#define PConfInpF(pin)		PinConfInput(PFPAR, DDRF, pin)
#define PConfBusF(pin)		PinConfBus(PFPAR, pin)
#define PInpToOutF(pin)		PinConfInputToOutput(PORTF, PFPAR, DDRF, pin)

#else

void PSetF0(void) = { BSETBI, 0, (ushort) PORTF };
	#define	PSetf0	PSetF0
void PClearF0(void) = { BCLRBI, 0, (ushort) PORTF };
	#define	PClearf0	PClearF0
void PChangeF0(void) = { BCHGBI, 0, (ushort) PORTF };
	#define	PChangef0	PChangeF0
int PinF0(void) = { BTSTBI, 0, (ushort) PORTF, SNENEGEXTWD0 };
	#define	Pinf0	PinF0
void PConfOutpF0(void) = { BCLRBI, 0, (ushort) PFPAR, BSETBI, 0, (ushort) DDRF };
	#define	PConfOutpf0	PConfOutpF0
void PConfInpF0(void) = { BCLRBI, 0, (ushort) PFPAR, BCLRBI, 0, (ushort) DDRF };
	#define	PConfInpf0	PConfInpF0
void PConfBusF0(void) = { BSETBI, 0, (ushort) PFPAR };
	#define	PConfBusf0	PConfBusF0
void PInpToOutF0(void) = { BCLRBI, 0, (ushort) PFPAR, BCLRBI, 0, (ushort) DDRF, 
	MOVEBII, (ushort)PORTF&0xFFFF, (ushort)PORTF&0xFFFF, BSETBI, 0, (ushort) DDRF };
	#define	PInpToOutf0	PInpToOutF0

void PSetF1(void) = { BSETBI, 1, (ushort) PORTF };
	#define	PSetf1	PSetF1
void PClearF1(void) = { BCLRBI, 1, (ushort) PORTF };
	#define	PClearf1	PClearF1
void PChangeF1(void) = { BCHGBI, 1, (ushort) PORTF };
	#define	PChangef1	PChangeF1
int PinF1(void) = { BTSTBI, 1, (ushort) PORTF, SNENEGEXTWD0 };
	#define	Pinf1	PinF1
void PConfOutpF1(void) = { BCLRBI, 1, (ushort) PFPAR, BSETBI, 1, (ushort) DDRF };
	#define	PConfOutpf1	PConfOutpF1
void PConfInpF1(void) = { BCLRBI, 1, (ushort) PFPAR, BCLRBI, 1, (ushort) DDRF };
	#define	PConfInpf1	PConfInpF1
void PConfBusF1(void) = { BSETBI, 1, (ushort) PFPAR };
	#define	PConfBusf1	PConfBusF1
void PInpToOutF1(void) = { BCLRBI, 1, (ushort) PFPAR, BCLRBI, 1, (ushort) DDRF, 
	MOVEBII, (ushort)PORTF&0xFFFF, (ushort)PORTF&0xFFFF, BSETBI, 1, (ushort) DDRF };
	#define	PInpToOutf1	PInpToOutF1

void PSetF2(void) = { BSETBI, 2, (ushort) PORTF };
	#define	PSetf2	PSetF2
void PClearF2(void) = { BCLRBI, 2, (ushort) PORTF };
	#define	PClearf2	PClearF2
void PChangeF2(void) = { BCHGBI, 2, (ushort) PORTF };
	#define	PChangef2	PChangeF2
int PinF2(void) = { BTSTBI, 2, (ushort) PORTF, SNENEGEXTWD0 };
	#define	Pinf2	PinF2
void PConfOutpF2(void) = { BCLRBI, 2, (ushort) PFPAR, BSETBI, 2, (ushort) DDRF };
	#define	PConfOutpf2	PConfOutpF2
void PConfInpF2(void) = { BCLRBI, 2, (ushort) PFPAR, BCLRBI, 2, (ushort) DDRF };
	#define	PConfInpf2	PConfInpF2
void PConfBusF2(void) = { BSETBI, 2, (ushort) PFPAR };
	#define	PConfBusf2	PConfBusF2
void PInpToOutF2(void) = { BCLRBI, 2, (ushort) PFPAR, BCLRBI, 2, (ushort) DDRF, 
	MOVEBII, (ushort)PORTF&0xFFFF, (ushort)PORTF&0xFFFF, BSETBI, 2, (ushort) DDRF };
	#define	PInpToOutf2	PInpToOutF2

void PSetF3(void) = { BSETBI, 3, (ushort) PORTF };
	#define	PSetf3	PSetF3
void PClearF3(void) = { BCLRBI, 3, (ushort) PORTF };
	#define	PClearf3	PClearF3
void PChangeF3(void) = { BCHGBI, 3, (ushort) PORTF };
	#define	PChangef3	PChangeF3
int PinF3(void) = { BTSTBI, 3, (ushort) PORTF, SNENEGEXTWD0 };
	#define	Pinf3	PinF3
void PConfOutpF3(void) = { BCLRBI, 3, (ushort) PFPAR, BSETBI, 3, (ushort) DDRF };
	#define	PConfOutpf3	PConfOutpF3
void PConfInpF3(void) = { BCLRBI, 3, (ushort) PFPAR, BCLRBI, 3, (ushort) DDRF };
	#define	PConfInpf3	PConfInpF3
void PConfBusF3(void) = { BSETBI, 3, (ushort) PFPAR };
	#define	PConfBusf3	PConfBusF3
void PInpToOutF3(void) = { BCLRBI, 3, (ushort) PFPAR, BCLRBI, 3, (ushort) DDRF, 
	MOVEBII, (ushort)PORTF&0xFFFF, (ushort)PORTF&0xFFFF, BSETBI, 3, (ushort) DDRF };
	#define	PInpToOutf3	PInpToOutF3

void PSetF4(void) = { BSETBI, 4, (ushort) PORTF };
	#define	PSetf4	PSetF4
void PClearF4(void) = { BCLRBI, 4, (ushort) PORTF };
	#define	PClearf4	PClearF4
void PChangeF4(void) = { BCHGBI, 4, (ushort) PORTF };
	#define	PChangef4	PChangeF4
int PinF4(void) = { BTSTBI, 4, (ushort) PORTF, SNENEGEXTWD0 };
	#define	Pinf4	PinF4
void PConfOutpF4(void) = { BCLRBI, 4, (ushort) PFPAR, BSETBI, 4, (ushort) DDRF };
	#define	PConfOutpf4	PConfOutpF4
void PConfInpF4(void) = { BCLRBI, 4, (ushort) PFPAR, BCLRBI, 4, (ushort) DDRF };
	#define	PConfInpf4	PConfInpF4
void PConfBusF4(void) = { BSETBI, 4, (ushort) PFPAR };
	#define	PConfBusf4	PConfBusF4
void PInpToOutF4(void) = { BCLRBI, 4, (ushort) PFPAR, BCLRBI, 4, (ushort) DDRF, 
	MOVEBII, (ushort)PORTF&0xFFFF, (ushort)PORTF&0xFFFF, BSETBI, 4, (ushort) DDRF };
	#define	PInpToOutf4	PInpToOutF4

void PSetF5(void) = { BSETBI, 5, (ushort) PORTF };
	#define	PSetf5	PSetF5
void PClearF5(void) = { BCLRBI, 5, (ushort) PORTF };
	#define	PClearf5	PClearF5
void PChangeF5(void) = { BCHGBI, 5, (ushort) PORTF };
	#define	PChangef5	PChangeF5
int PinF5(void) = { BTSTBI, 5, (ushort) PORTF, SNENEGEXTWD0 };
	#define	Pinf5	PinF5
void PConfOutpF5(void) = { BCLRBI, 5, (ushort) PFPAR, BSETBI, 5, (ushort) DDRF };
	#define	PConfOutpf5	PConfOutpF5
void PConfInpF5(void) = { BCLRBI, 5, (ushort) PFPAR, BCLRBI, 5, (ushort) DDRF };
	#define	PConfInpf5	PConfInpF5
void PConfBusF5(void) = { BSETBI, 5, (ushort) PFPAR };
	#define	PConfBusf5	PConfBusF5
void PInpToOutF5(void) = { BCLRBI, 5, (ushort) PFPAR, BCLRBI, 5, (ushort) DDRF, 
	MOVEBII, (ushort)PORTF&0xFFFF, (ushort)PORTF&0xFFFF, BSETBI, 5, (ushort) DDRF };
	#define	PInpToOutf5	PInpToOutF5

void PSetF6(void) = { BSETBI, 6, (ushort) PORTF };
	#define	PSetf6	PSetF6
void PClearF6(void) = { BCLRBI, 6, (ushort) PORTF };
	#define	PClearf6	PClearF6
void PChangeF6(void) = { BCHGBI, 6, (ushort) PORTF };
	#define	PChangef6	PChangeF6
int PinF6(void) = { BTSTBI, 6, (ushort) PORTF, SNENEGEXTWD0 };
	#define	Pinf6	PinF6
void PConfOutpF6(void) = { BCLRBI, 6, (ushort) PFPAR, BSETBI, 6, (ushort) DDRF };
	#define	PConfOutpf6	PConfOutpF6
void PConfInpF6(void) = { BCLRBI, 6, (ushort) PFPAR, BCLRBI, 6, (ushort) DDRF };
	#define	PConfInpf6	PConfInpF6
void PConfBusF6(void) = { BSETBI, 6, (ushort) PFPAR };
	#define	PConfBusf6	PConfBusF6
void PInpToOutF6(void) = { BCLRBI, 6, (ushort) PFPAR, BCLRBI, 6, (ushort) DDRF, 
	MOVEBII, (ushort)PORTF&0xFFFF, (ushort)PORTF&0xFFFF, BSETBI, 6, (ushort) DDRF };
	#define	PInpToOutf6	PInpToOutF6

void PSetF7(void) = { BSETBI, 7, (ushort) PORTF };
	#define	PSetf7	PSetF7
void PClearF7(void) = { BCLRBI, 7, (ushort) PORTF };
	#define	PClearf7	PClearF7
void PChangeF7(void) = { BCHGBI, 7, (ushort) PORTF };
	#define	PChangef7	PChangeF7
int PinF7(void) = { BTSTBI, 7, (ushort) PORTF, SNENEGEXTWD0 };
	#define	Pinf7	PinF7
void PConfOutpF7(void) = { BCLRBI, 7, (ushort) PFPAR, BSETBI, 7, (ushort) DDRF };
	#define	PConfOutpf7	PConfOutpF7
void PConfInpF7(void) = { BCLRBI, 7, (ushort) PFPAR, BCLRBI, 7, (ushort) DDRF };
	#define	PConfInpf7	PConfInpF7
void PConfBusF7(void) = { BSETBI, 7, (ushort) PFPAR };
	#define	PConfBusf7	PConfBusF7
void PInpToOutF7(void) = { BCLRBI, 7, (ushort) PFPAR, BCLRBI, 7, (ushort) DDRF, 
	MOVEBII, (ushort)PORTF&0xFFFF, (ushort)PORTF&0xFFFF, BSETBI, 7, (ushort) DDRF };
	#define	PInpToOutf7	PInpToOutF7

#endif /* __GNUC__ */

#endif	/*	__dio332_H */

