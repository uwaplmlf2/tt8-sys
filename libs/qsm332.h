/*******************************************************************************
**	qsm332.h -- 68332 Tattletale (7,8) Queued Serial Module Definitions
**
**          Copyright 1994 ONSET Computer Corp.  All rights reserved.
**	
**	94/02/11 --jhg
**		Adapted from TT7 "qsm.h", "qspi.h", & "sci.h"
*******************************************************************************/

#ifndef		__qsm332_H
#define		__qsm332_H

#include	<tat332.h>

/****************************************************************************/

#define		M_QSTOP		0x8000	/* 1 = QSM clock stopped */
#define		M_FRZ1		0x4000	/* 1 = halt QSM @ FREEZE */
#define		M_FRZ0		0x2000	/* reserved for future	*/
#define		M_QSUPV		0x0080	/* 1 = restrict access	*/
#define		M_QIARB		0x0004	/* interrupt arbitration bits */

struct	QMCR_			/* QSM CONFIGURATION REGISTER (QMCR)  [SIM-5-11]	*/
	{
#ifdef FIRST_BF_8000
	unsint		QSTOP	:	1;
	unsint		FRZ1	:	1;
	unsint		FRZ0	:	1;
	unsint				:	5;
	unsint		QSUPV	:	1;
	unsint				:	3;
	unsint		QIARB	:	4;
#endif
#ifdef FIRST_BF_0001
	unsint		QIARB	:	4;
	unsint				:	3;
	unsint		QSUPV	:	1;
	unsint				:	5;
	unsint		FRZ0	:	1;
	unsint		FRZ1	:	1;
	unsint		QSTOP	:	1;
#endif
	};
#define		QMCR			((uspv) (IMBASE + 0xC00))
#define		_QMCR			((struct QMCR_ *) QMCR)


/****************************************************************************/
					/* QSM INTERRUPT LEVEL REGISTER 0 (QILR)  [SIM-5-13]	*/
#define		QILR				((ucpv) (IMBASE + 0xC04))
	

/****************************************************************************/
					/* QSM INTERRUPT VECTOR REGISTER 0 (QVLR)  [SIM-5-14]	*/
#define		QVLR				((ucpv) (IMBASE + 0xC05))
	

/****************************************************************************/
struct	QPDR_			/* QSM PIN DATA REGISTER (QPDR)  [SIM-5-15] */
	{
#ifdef FIRST_BF_8000
	unsint			:	8;
	unsint		TXD	:	1;	/* pin 7	(TXD) */
	unsint		PCS3	:	1;	/* pin 6	(PCS3) */
	unsint		PCS2	:	1;	/* pin 5	(PCS2) */
	unsint		PCS1	:	1;	/* pin 4	(PCS1) */
	unsint		PCS0	:	1;	/* pin 3	(PCS0) */
	unsint		SCK	:	1;	/* pin 2	(SCK) */
	unsint		MOSI	:	1;	/* pin 1	(MOSI) */
	unsint		MISO	:	1;	/* pin 0	(MISO) */
#endif
#ifdef FIRST_BF_0001
	unsint		MISO	:	1;	/* pin 0	(MISO) */
	unsint		MOSI	:	1;	/* pin 1	(MOSI) */
	unsint		SCK	:	1;	/* pin 2	(SCK) */
	unsint		PCS0	:	1;	/* pin 3	(PCS0) */
	unsint		PCS1	:	1;	/* pin 4	(PCS1) */
	unsint		PCS2	:	1;	/* pin 5	(PCS2) */
	unsint		PCS3	:	1;	/* pin 6	(PCS3) */
	unsint		TXD	:	1;	/* pin 7	(TXD) */
	unsint			:	8;
#endif

	};
#define		QPDR			((ucpv) (IMBASE + 0xC15))
#define		_QPDR			((volatile struct QPDR_ *) (IMBASE + 0xC14))


/****************************************************************************/
struct	QPAR_			/* QSM PIN ASSIGNMENT REGISTER (QPAR)  [SIM-5-15] */
	{
#ifdef FIRST_BF_8000
	unsint			:	1;
	unsint		PCS3	:	1;	/* pin 6	(PCS3) */
	unsint		PCS2	:	1;	/* pin 5	(PCS2) */
	unsint		PCS1	:	1;	/* pin 4	(PCS1) */
	unsint		PCS0	:	1;	/* pin 3	(PCS0) */
	unsint			:	1;
	unsint		MOSI	:	1;	/* pin 1	(MOSI) */
	unsint		MISO	:	1;	/* pin 0	(MISO) */
	unsint			:	8;
#endif
#ifdef FIRST_BF_0001
	unsint			:	8;
	unsint		MISO	:	1;	/* pin 0	(MISO) */
	unsint		MOSI	:	1;	/* pin 1	(MOSI) */
	unsint			:	1;
	unsint		PCS0	:	1;	/* pin 3	(PCS0) */
	unsint		PCS1	:	1;	/* pin 4	(PCS1) */
	unsint		PCS2	:	1;	/* pin 5	(PCS2) */
	unsint		PCS3	:	1;	/* pin 6	(PCS3) */
	unsint			:	1;
#endif
	};
#define		QPAR			((ucpv) (IMBASE + 0xC16))
#define		_QPAR			((struct QPAR_ *) QPAR)


/****************************************************************************/
struct	QDDR_			/* QSM DATA DIRECTION REGISTER (QDDR)  [SIM-5-16] */
	{
#ifdef FIRST_BF_8000
	unsint			:	8;
	unsint		TXD	:	1;	/* pin 7	(TXD) */
	unsint		PCS3	:	1;	/* pin 6	(PCS3) */
	unsint		PCS2	:	1;	/* pin 5	(PCS2) */
	unsint		PCS1	:	1;	/* pin 4	(PCS1) */
	unsint		PCS0	:	1;	/* pin 3	(PCS0) */
	unsint		SCK	:	1;	/* pin 2	(SCK) */
	unsint		MOSI	:	1;	/* pin 1	(MOSI) */
	unsint		MISO	:	1;	/* pin 0	(MISO) */
#endif
#ifdef FIRST_BF_0001
	unsint		MISO	:	1;	/* pin 0	(MISO) */
	unsint		MOSI	:	1;	/* pin 1	(MOSI) */
	unsint		SCK	:	1;	/* pin 2	(SCK) */
	unsint		PCS0	:	1;	/* pin 3	(PCS0) */
	unsint		PCS1	:	1;	/* pin 4	(PCS1) */
	unsint		PCS2	:	1;	/* pin 5	(PCS2) */
	unsint		PCS3	:	1;	/* pin 6	(PCS3) */
	unsint		TXD	:	1;	/* pin 7	(TXD) */
	unsint			:	8;
#endif
	};
#define		QDDR			((ucpv) (IMBASE + 0xC17))
#define		_QDDR			((struct QDDR_ *) (IMBASE + 0xC16))

/****************************************************************************/
/*					MASKS FOR QPDR, QPAR, AND QDDR							*/
#define			M_TXD		0x80		/* pin 7	(TXD)					*/
#define			M_PCS3		0x40		/* pin 6	(PCS3)					*/
#define			M_PCS2		0x20		/* pin 5	(PCS2)					*/
#define			M_PCS1		0x10		/* pin 4	(PCS1)					*/
#define			M_PCS0		0x08		/* pin 3	(PCS0)					*/
#define			M_SCK		0x04		/* pin 2	(SCK)					*/
#define			M_MOSI		0x02		/* pin 1	(MOSI)					*/
#define			M_MISO		0x01		/* pin 0	(MISO)					*/


/****************************************************************************/

#define	M_MSTR		0x8000	/* 1 = Master, 0 = Slave		*/
#define	M_WOMQ		0x4000	/* 1 = open drain, 0 = cmos		*/
#define	M_CPOL		0x0200	/* 1 = inactive SCK high		*/
#define	M_CPHA		0x0100	/* 1 = chg lding, capt folng	*/

struct	SPCR0_				/* QSPI CONTROL REGISTER 0 (SPCR0)  [SIM-5-22]	*/
	{
#ifdef FIRST_BF_8000
	unsint		MSTR	:	1;
	unsint		WOMQ	:	1;
	unsint		BITS	:	4;
	unsint		CPOL	:	1;
	unsint		CPHA	:	1;
	unsint		BAUD	:	8;
#endif
#ifdef FIRST_BF_0001
	unsint		BAUD	:	8;
	unsint		CPHA	:	1;
	unsint		CPOL	:	1;
	unsint		BITS	:	4;
	unsint		WOMQ	:	1;
	unsint		MSTR	:	1;
#endif
	};
#define		SPCR0				((uspv) (IMBASE + 0xC18))
#define		_SPCR0				((struct SPCR0_ *) SPCR0)


/****************************************************************************/

#define	M_SPE	0x8000		/* QSPI enable = 1, 0 = I/O		*/

struct	SPCR1_				/* QSPI CONTROL REGISTER 1 (SPCR1)  [SIM-5-25]	*/
	{
#ifdef FIRST_BF_8000
	unsint		SPE		:	1;
	unsint		DSCKL	:	7;
	unsint		DTL		:	8;
#endif
#ifdef FIRST_BF_0001
	unsint		DTL		:	8;
	unsint		DSCKL	:	7;
	unsint		SPE		:	1;
#endif
	};
#define		SPCR1				((uspv) (IMBASE + 0xC1A))
#define		_SPCR1				((struct SPCR1_ *) SPCR1)


/****************************************************************************/

#define	M_SPIFIE	0x8000	/* SPI finished rupt	= 1		*/
#define	M_WREN		0x4000	/* wraparound enable = 1		*/
#define	M_WRTO		0x2000	/* wrap to */

struct	SPCR2_				/* QSPI CONTROL REGISTER 2 (SPCR2)  [SIM-5-25]	*/
	{
#ifdef FIRST_BF_8000
	unsint		SPIFIE	:	1;
	unsint		WREN	:	1;
	unsint		WRTO	:	1;
	unsint				:	1;
	unsint		ENDQP	:	4;			/* Ending Queue Pointer			*/
	unsint				:	4;
	unsint		NEWQP	:	4;			/* New Queue Pointer			*/
#endif
#ifdef FIRST_BF_0001
	unsint		NEWQP	:	4;			/* New Queue Pointer			*/
	unsint				:	4;
	unsint		ENDQP	:	4;			/* Ending Queue Pointer			*/
	unsint				:	1;
	unsint		WRTO	:	1;
	unsint		WREN	:	1;
	unsint		SPIFIE	:	1;
#endif
	};
#define		SPCR2				((uspv) (IMBASE + 0xC1C))
#define		_SPCR2				((struct SPCR2_ *) SPCR2)


/****************************************************************************/

#define M_LOOPQ 	0x04	/* QSPI loop mode, 1=on			*/
#define	M_HMIE		0x02	/* HALTA & MODF rupt enable		*/
#define	M_HALT		0x01	/* Halt	*/


struct	SPCR3_				/* QSPI CONTROL REGISTER 3 (SPCR3)  [SIM-5-29]	*/
	{
#ifdef FIRST_BF_8000
	unsint				:	5;
	unsint		LOOPQ	:	1;
	unsint		HMIE	:	1;
	unsint		HALT	:	1;
	unsint				:	8;
#endif
#ifdef FIRST_BF_0001
	unsint				:	8;
	unsint		HALT	:	1;
	unsint		HMIE	:	1;
	unsint		LOOPQ	:	1;
	unsint				:	5;
#endif
	};
#define		SPCR3				((ucpv) (IMBASE + 0xC1E))
#define		_SPCR3				((struct SPCR3_ *) (IMBASE + 0xC1E))


/****************************************************************************/

#define M_SPIF	0x80		/* QSPI finished flag			*/
#define	M_MODF	0x40		/* QSPI mode fault flag			*/
#define	M_HALTA	0x20		/* QSPI halt ack flag			*/

struct	SPSR_				/* QSPI STATUS REGISTER (SPSR)  [SIM-5-30]		*/
	{
#ifdef FIRST_BF_8000
	unsint				:	8;
	unsint		SPIF	:	1;
	unsint		MODF	:	1;
	unsint		HALTA	:	1;
	unsint				:	1;
	unsint		CPTQP	:	4;			/* Completed Queue Pointer		*/
#endif
#ifdef FIRST_BF_0001
	unsint		CPTQP	:	4;			/* Completed Queue Pointer		*/
	unsint				:	1;
	unsint		HALTA	:	1;
	unsint		MODF	:	1;
	unsint		SPIF	:	1;
	unsint				:	8;
#endif
	};
#define		SPSR				((ucpv) (IMBASE + 0xC1F))
#define		_SPSR				((struct SPSR_ *) (IMBASE + 0xC1E))


/****************************************************************************/
							/* SCI CONTROL REGISTER 0 (SPIRCV)  [SIM-5-33]	*/
#define		SPIRCV				((uspv) (IMBASE + 0xD00))
	

/****************************************************************************/
							/* SCI CONTROL REGISTER 0 (SPIXMT)  [SIM-5-33]	*/
#define		SPIXMT				((uspv) (IMBASE + 0xD20))
	

/****************************************************************************/
						/* QSPI COMMAND CONTROL RAM (SPICMD)  [SIM-5-33]	*/
	#define		M_CONT		0x80		/* 1=keep CS active				*/
	#define		M_BITSE		0x40		/* 0=8 bits, 1=use SPCR0		*/
	#define		M_DT		0x20		/* 0=no delay after,1=use SPCR1	*/
	#define		M_DSCK		0x10		/* 0=1/2SCK, 1=use SPCR1		*/
	#define		M_CS3		0x08		/* chip select 3				*/
	#define		M_CS2		0x04		/* chip select 2				*/
	#define		M_CS1		0x02		/* chip select 1				*/
	#define		M_CS0		0x01		/* chip select 0				*/
#define		SPICMD				((ucpv) (IMBASE + 0xD40))
#define		SPICMDARRAY			((uchar *) (IMBASE + 0xD40))

/****************************************************************************/
							/* SCI CONTROL REGISTER 0 (SCCR0)  [SIM-5-56]	*/
#define		SCCR0				((uspv) (IMBASE + 0xC08))
	

/****************************************************************************/

#define	M_LOOPS	0x4000		/* 1 = test with loop enabled	*/
#define	M_WOMS	0x2000		/* 1 = TxD open drain, 0 = CMOS */
#define	M_ILT	0x1000		/* 1 = long idle line detect	*/
#define	M_PT	0x0800		/* 1 = odd parity, 0 = even		*/
#define	M_PE	0x0400		/* 1 = parity enabled			*/
#define	M_M	0x0200		/* 1 = 1s,9d,1s,  0 = 1s,8d,1s	*/
#define	M_WAKE	0x0100		/* 1 = wake addr mark, 0 = idle */
#define	M_TIE	0x0080		/* 1 = enable TDRE interrupts	*/
#define	M_TCIE	0x0040		/* 1 = enable TC interrupts		*/
#define	M_RIE	0x0020		/* 1 = enable RDRF interrupts	*/
#define	M_ILIE	0x0010		/* 1 = enable IDLE interrupts	*/
#define	M_TE	0x0008		/* 1 = TxD enabled, 0 = gp I/O	*/
#define	M_RE	0x0004		/* 1 = receiver enabled 		*/
#define	M_RWU	0x0002		/* 1 = wakeup mode enabled		*/
#define	M_SBK	0x0001		/* 1 = send break			*/

struct	SCCR1_				/* SCI CONTROL REGISTER 1 (SCCR1)  [SIM-5-58]	*/
	{
#ifdef FIRST_BF_8000
	unsint				:	1;
	unsint		LOOPS	:	1;
	unsint		WOMS	:	1;
	unsint		ILT		:	1;
	unsint		PT		:	1;
	unsint		PE		:	1;
	unsint		M		:	1;
	unsint		WAKE	:	1;
	unsint		TIE		:	1;
	unsint		TCIE	:	1;
	unsint		RIE		:	1;
	unsint		ILIE	:	1;
	unsint		TE		:	1;
	unsint		RE		:	1;
	unsint		RWU		:	1;
	unsint		SBK		:	1;
#endif
#ifdef FIRST_BF_0001
	unsint		SBK		:	1;
	unsint		RWU		:	1;
	unsint		RE		:	1;
	unsint		TE		:	1;
	unsint		ILIE	:	1;
	unsint		RIE		:	1;
	unsint		TCIE	:	1;
	unsint		TIE		:	1;
	unsint		WAKE	:	1;
	unsint		M		:	1;
	unsint		PE		:	1;
	unsint		PT		:	1;
	unsint		ILT		:	1;
	unsint		WOMS	:	1;
	unsint		LOOPS	:	1;
	unsint				:	1;
#endif
	};
#define		SCCR1				((uspv) (IMBASE + 0xC0A))
#define		_SCCR1				((struct SCCR1_ *) SCCR1)


/****************************************************************************/

#define	M_TDRE	0x0100		/* Transmit Data Register Empty	*/
#define	M_TC	0x0080		/* Transmit Complete Flag		*/
#define	M_RDRF	0x0040		/* Receive Data Register Full	*/
#define	M_RAF	0x0020		/* Receiver Active Flag			*/
#define	M_IDLE	0x0010		/* Idle-Line Detected Flag		*/
#define	M_OR	0x0008		/* Overrun Error Flag			*/
#define	M_NF	0x0004		/* Noise Error Flag			*/
#define	M_FE	0x0002		/* Framing Error Flag			*/
#define	M_PF	0x0001		/* Parity Error Flag			*/

struct	SCSR_					/* SCI STATUS REGISTER (SCSR)  [SIM-5-62]	*/
	{
#ifdef FIRST_BF_8000
	unsint				:	7;
	unsint		TDRE	:	1;
	unsint		TC		:	1;
	unsint		RDRF	:	1;
	unsint		RAF		:	1;
	unsint		IDLE	:	1;
	unsint		OR		:	1;
	unsint		NF		:	1;
	unsint		FE		:	1;
	unsint		PF		:	1;
#endif
#ifdef FIRST_BF_0001
	unsint		PF		:	1;
	unsint		FE		:	1;
	unsint		NF		:	1;
	unsint		OR		:	1;
	unsint		IDLE	:	1;
	unsint		RAF		:	1;
	unsint		RDRF	:	1;
	unsint		TC		:	1;
	unsint		TDRE	:	1;
	unsint				:	7;
#endif
	};
#define		SCSR				((uspv) (IMBASE + 0xC0C))
#define		_SCSR				((struct SCSR_ *) SCSR)


/****************************************************************************/
							/* SCI DATA REGISTER 0 (SCDR)  [SIM-5-65]		*/
#define		SCDR				((uspv) (IMBASE + 0xC0E))


#endif	/*	__qsm332_H */

