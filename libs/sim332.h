/*******************************************************************************
**	sim332.h -- 68332 Tattletale (7,8) System Integration Module Definitions
**
**          Copyright 1994 ONSET Computer Corp.  All rights reserved.
**	
**	94/02/11 --jhg
**		Adapted from TT7 "sim.h"
*******************************************************************************/

#ifndef		__sim332_H
#define		__sim332_H

#include	<tat332.h>

/****************************************************************************/
#define	M_EXOFF		0x8000	/* 1 = clkout high impedance		*/
#define	M_FRZSW		0x4000	/* 1 = disabl wtchdg & pit @ FREEZE	*/
#define	M_FRZBM		0x2000	/* 1 = disable bus monitor @ FREEZE	*/
#define	M_SLVEN		0x0800	/* 1 = ext masters ctrll int periph	*/
#define	M_SHEN1		0x0200	/* \	show cycle enables		*/
#define	M_SHEN0		0x0100	/* /					*/
#define	M_MCRSUPV	0x0080	/* 1 = restrict access when FC2 = 1	*/
#define	M_MM		0x0040	/* 1 = map to $fffxxx, 0 = $7ffxxx	*/
#define	M_IARB		0x000F	/* interrupt arbitration bits		*/


struct	MCR_	/* MODULE CONFIGURATON REGISTER 1 (MCR)			[SIM-4-6]	*/
	{
#ifdef FIRST_BF_8000
	unsint		EXOFF	:	1;
	unsint		FRZSW	:	1;
	unsint		FRZBM	:	1;
	unsint				:	1;
	unsint		SLVEN	:	1;
	unsint				:	1;
	unsint		SHEN1	:	1;
	unsint		SHEN0	:	1;
	unsint		MCRSUPV	:	1;
	unsint		MM		:	1;
	unsint				:	2;
	unsint		IARB	:	4;
#endif
#ifdef FIRST_BF_0001
	unsint		IARB	:	4;
	unsint				:	2;
	unsint		MM		:	1;
	unsint		MCRSUPV	:	1;
	unsint		SHEN0	:	1;
	unsint		SHEN1	:	1;
	unsint				:	1;
	unsint		SLVEN	:	1;
	unsint				:	1;
	unsint		FRZBM	:	1;
	unsint		FRZSW	:	1;
	unsint		EXOFF	:	1;
#endif
	};
#define		MCR				((uspv) (IMBASE + 0xA00))
#define		_MCR			((struct MCR_ *) MCR)


/****************************************************************************/

#define	M_EXT		0x80	/* external			*/
#define	M_POW		0x40	/* power up			*/
#define	M_SW		0x20	/* watchdog			*/
#define	M_HLT		0x10	/* halt monitor	 		*/
#define	M_LOC		0x04	/* clock loss			*/
#define	M_SYS		0x02	/* reset instruction		*/
#define	M_TST		0x01	/* test submodule		*/

struct	RSR_				/* RESET STATUS REGISTER (RSR)  [SIM-4-9]		*/
	{
#ifdef FIRST_BF_8000
	unsint				:	8;
	unsint		EXT		:	1;
	unsint		POW		:	1;
	unsint		SW		:	1;
	unsint		HLT		:	1;
	unsint				:	1;
	unsint		LOC		:	1;
	unsint		SYS		:	1;
	unsint		TST		:	1;
#endif
#ifdef FIRST_BF_0001
	unsint		TST		:	1;
	unsint		SYS		:	1;
	unsint		LOC		:	1;
	unsint				:	1;
	unsint		HLT		:	1;
	unsint		SW		:	1;
	unsint		POW		:	1;
	unsint		EXT		:	1;
	unsint				:	8;
#endif
	};
#define		RSR				((ucpv) (IMBASE + 0xA07))
#define		_RSR			((struct RSR_ *) (IMBASE + 0xA06))


/****************************************************************************/

#define		M_SWE		0x80	/* enable watchdog*/
#define		M_SWP		0x40	/* watchdog prescale*/
#define		M_SWTI		0x20	/* \ watchdog timing*/
#define		M_SWT0		0x10	/* / */
#define		M_HME		0x08	/* enable halt monitor*/
#define		M_BME		0x04	/* enable bus monitor*/
#define		M_BMT1		0x02	/* \ bus monitor timing*/
#define		M_BMT0		0x01	/* / */

struct	SYPCR_	/* SYSTEM PROTECTION CONTROL REGISTER (SYPCR)  [SIM-4-10]	*/
	{
#ifdef FIRST_BF_8000
	unsint				:	8;
	unsint		SWE		:	1;
	unsint		SWP		:	1;
	unsint		SWTI	:	1;
	unsint		SWT0	:	1;
	unsint		HME		:	1;
	unsint		BME		:	1;
	unsint		BMT1	:	1;
	unsint		BMT0	:	1;
#endif  
#ifdef FIRST_BF_0001
	unsint		BMT0	:	1;
	unsint		BMT1	:	1;
	unsint		BME		:	1;
	unsint		HME		:	1;
	unsint		SWT0	:	1;
	unsint		SWTI	:	1;
	unsint		SWP		:	1;
	unsint		SWE		:	1;
	unsint				:	8;
#endif
	};
#define		SYPCR			((ucpv) (IMBASE + 0xA21))
#define		_SYPCR			((struct SYPCR_ *) (IMBASE + 0xA20))


/****************************************************************************/
							/* SOFTWARE SERVICE REGISTER (SSR)  [SIM-4-14]	*/
#define		SSR						((ucpv) (IMBASE + 0xA27))
#define		ServiceWatchdog()		do { *SSR = 0x55; *SSR = 0xaa; } while (0)


/****************************************************************************/

#define		M_PTP		0x0100	/* mask for prescale (1 = /512)	*/

struct	PITR_				/* PERIODIC INTERRUPT TIMER (PITR)  [SIM-4-14]	*/
	{
#ifdef FIRST_BF_8000
	unsint				:	7;
	unsint		PTP		:	1;
	unsint		PITR	:	8;
#endif
#ifdef FIRST_BF_0001
	unsint		PITR	:	8;
	unsint		PTP		:	1;
	unsint				:	7;
#endif
	};
#define		PITR			((uspv) (IMBASE + 0xA24))
#define		_PITR			((struct PITR_ *) PITR)


/****************************************************************************/
struct	PICR_	/* PERIODIC INTERRUPT CONTROL REGISTER (PICR)  [SIM-4-15]	*/
	{
#ifdef FIRST_BF_8000
	unsint				:	5;
	unsint		PIRQL	:	3;
	unsint		PIV		:	8;
#endif
#ifdef FIRST_BF_0001
	unsint		PIV		:	8;
	unsint		PIRQL	:	3;
	unsint				:	5;
#endif
	};
#define		PICR			((uspv) (IMBASE + 0xA22))
#define		_PICR			((struct PICR_ *) PICR)


/****************************************************************************/

#define		M_W	0x8000	/* VCO * 4				*/
#define		M_X	0x4000	/* SYSCLOCK * 2				*/
#define		M_EDIV	0x0080	/* 1 = /16, 0 = /8			*/
#define		M_SLIMP	0x0010	/* xtal ref lost, running half max	*/
#define		M_SLOCK	0x0008	/* VCO locked				*/
#define		M_RSTEN	0x0004	/* 1 = reset if crystal lost		*/
#define		M_STSIM	0x0002	/* LPSTOP drives SIM from VCO		*/
#define		M_STEXT	0x0001	/* LPSTOP enables external clock */


struct	SYNCR_	/* CLOCK SYNTHESIZER CONTROL REGISTER (SYNCR)  [SIM-4-21]	*/
	{
#ifdef FIRST_BF_8000
	unsint		W		:	1;

	unsint		X		:	1;

	unsint		Y		:	6;
	unsint		EDIV	:	1;

	unsint				:	2;
	unsint		SLIMP	:	1;

	unsint		SLOCK	:	1;

	unsint		RSTEN	:	1;

	unsint		STSIM	:	1;

	unsint		STEXT	:	1;

#endif
#ifdef FIRST_BF_0001
	unsint		STEXT	:	1;

	unsint		STSIM	:	1;

	unsint		RSTEN	:	1;

	unsint		SLOCK	:	1;

	unsint		SLIMP	:	1;

	unsint				:	2;
	unsint		EDIV	:	1;

	unsint		Y		:	6;
	unsint		X		:	1;

	unsint		W		:	1;

#endif

	};
#define		SYNCR			((uspv) (IMBASE + 0xA04))
#define		_SYNCR			((volatile struct SYNCR_ *) SYNCR)
#define		FSYS(n)			(((((ulong) (n)) / (FCRYSTAL * 4L)) - 1L) << 8)
#define		SYSFREQ			((FCRYSTAL*4)*((_SYNCR->Y+1)<<(*(uchar *)SYNCR>>6)))


/****************************************************************************/

#define	M_CS5		0x3000
#define	M_CS4		0x0C00
#define	M_CSS3		0x0300	/* name collision with M_CS3 (QSPI)	*/
#define	M_CSS2		0x00C0	/* name collision with M_CS2 (QSPI)	*/
#define	M_CSS1		0x0030	/* name collision with M_CS1 (QSPI)	*/
#define	M_CSS0		0x000C	/* name collision with M_CS0 (QSPI)	*/
#define	M_CSBOOT	0x0003

struct	CSPAR0_	/* CHIP SELECT PIN ASSIGNMENT REGISTER (CSPAR0)  [SIM-4-32]	*/
	{
#ifdef FIRST_BF_8000
	unsint				:	2;
	unsint		CS5		:	2;	/* CS 5 (FC2)								*/

	unsint		CS4		:	2;	/* CS 4 (FC1)								*/

	unsint		CS3		:	2;	/* CS 3 (FC0)								*/

	unsint		CS2		:	2;	/* CS 2 (BGACK)								*/

	unsint		CS1		:	2;	/* CS 1 (BG)								*/

	unsint		CS0		:	2;	/* CS 0 (BR)								*/

	unsint		CSBOOT	:	2;	/* CSBOOT									*/

#endif
#ifdef FIRST_BF_0001
	unsint		CSBOOT	:	2;	/* CSBOOT									*/

	unsint		CS0		:	2;	/* CS 0 (BR)								*/

	unsint		CS1		:	2;	/* CS 1 (BG)								*/

	unsint		CS2		:	2;	/* CS 2 (BGACK)								*/

	unsint		CS3		:	2;	/* CS 3 (FC0)								*/

	unsint		CS4		:	2;	/* CS 4 (FC1)								*/

	unsint		CS5		:	2;	/* CS 5 (FC2)								*/

	unsint				:	2;
#endif
	};
#define		CSPAR0			((uspv) (IMBASE + 0xA44))
#define		_CSPAR0			((struct CSPAR0_ *) CSPAR0)


/****************************************************************************/

#define		M_CS10		0x0300
#define		M_CS9		0x00C0
#define		M_CS8		0x0030
#define		M_CS7		0x000C
#define		M_CS6		0x0003


struct	CSPAR1_	/* CHIP SELECT PIN ASSIGNMENT REGISTER (CSPAR1)  [SIM-4-32]	*/
	{
#ifdef FIRST_BF_8000
	unsint				:	2;
	unsint				:	2;
	unsint				:	2;
	unsint		CS10	:	2;	/* CS 10 (A23)							*/

	unsint		CS9		:	2;	/* CS 9 (A22)							*/

	unsint		CS8		:	2;	/* CS 8 (A21)							*/

	unsint		CS7		:	2;	/* CS 7 (A20)							*/

	unsint		CS6		:	2;	/* CS 6 (A19)							*/

#endif
#ifdef FIRST_BF_0001
	unsint		CS6		:	2;	/* CS 6 (A19)							*/

	unsint		CS7		:	2;	/* CS 7 (A20)							*/

	unsint		CS8		:	2;	/* CS 8 (A21)							*/

	unsint		CS9		:	2;	/* CS 9 (A22)							*/

	unsint		CS10	:	2;	/* CS 10 (A23)							*/

	unsint				:	2;
	unsint				:	2;
	unsint				:	2;
#endif
	};
#define		CSPAR1			((uspv) (IMBASE + 0xA46))
#define		_CSPAR1			((struct CSPAR1_ *) CSPAR1)
#define		DISCRETE		0
#define		M_DISCRETE		0x0000
#define		DEFAULT			1
#define		M_DEFAULT		0x5555
#define		PORT8			2
#define		M_PORT8			0xAAAA
#define		PORT16			3
#define		M_PORT16		0xFFFF

#define		PARSEL

/****************************************************************************/
struct	CSBAR_	/* CHIP SELECT BASE ADDRESS REGISTERS (CSBAR)  [SIM-4-33]	*/
	{
#ifdef FIRST_BF_8000
	unsint		ADDR	:	13;
	unsint		SIZE	:	3;
#endif
#ifdef FIRST_BF_0001
	unsint		SIZE	:	3;
	unsint		ADDR	:	13;
#endif
	};
#define		CSBARBT		((uspv)	(IMBASE + 0xA48))
#define		_CSBARBT		((struct CSBAR_ *) CSBARBT)
#define		CSBAR0		((uspv)	(IMBASE + 0xA4C))
#define		_CSBAR0			((struct CSBAR_ *) CSBAR0)
#define		CSBAR1		((uspv)	(IMBASE + 0xA50))
#define		_CSBAR1			((struct CSBAR_ *) CSBAR1)
#define		CSBAR2		((uspv)	(IMBASE + 0xA54))
#define		_CSBAR2			((struct CSBAR_ *) CSBAR2)
#define		CSBAR3		((uspv)	(IMBASE + 0xA58))
#define		_CSBAR3			((struct CSBAR_ *) CSBAR3)
#define		CSBAR4		((uspv)	(IMBASE + 0xA5C))
#define		_CSBAR4			((struct CSBAR_ *) CSBAR4)
#define		CSBAR5		((uspv)	(IMBASE + 0xA60))
#define		_CSBAR5			((struct CSBAR_ *) CSBAR5)
#define		CSBAR6		((uspv)	(IMBASE + 0xA64))
#define		_CSBAR6			((struct CSBAR_ *) CSBAR6)
#define		CSBAR7		((uspv)	(IMBASE + 0xA68))
#define		_CSBAR7			((struct CSBAR_ *) CSBAR7)
#define		CSBAR8		((uspv)	(IMBASE + 0xA6C))
#define		_CSBAR8			((struct CSBAR_ *) CSBAR8)
#define		CSBAR9		((uspv)	(IMBASE + 0xA70))
#define		_CSBAR9			((struct CSBAR_ *) CSBAR9)
#define		CSBAR10		((uspv)	(IMBASE + 0xA74))
#define		_CSBAR10		((struct CSBAR_ *) CSBAR10)

#define		BLOCKSIZE(n) (0x2000L << ((n)&7)) /* ! only works for >= 65536 !*/
#define		BASEADDR(n)	 (((n) & 0xfff8L) << 8L)

/****************************************************************************/

#define	M_MDSYNC	0x8000		/* 0 = async, 1 = synchronous */
#define	M_BYTEU		0x4000		/* upper byte */
#define	M_BYTEL		0x2000		/* lower byte */
#define	M_CSWRITE	0x1000		/* write */
#define	M_CSREAD	0x0800		/* read */
#define	M_DSSTRB	0x0400		/* 0 = address, 1 = data strobe */
#define	M_WAIT0		0x0000		/* 0 wait */
#define	M_WAIT1		0x0040		/* 1 wait */
#define	M_WAIT2		0x0080		/* 2 wait */
#define	M_WAIT3		0x00c0		/* 3 wait */
#define	M_WAIT4		0x0100		/* 4 wait */
#define	M_WAIT5		0x0140		/* 5 wait */
#define	M_WAIT6		0x0180		/* 6 wait */
#define	M_WAIT7		0x01c0		/* 7 wait */
#define	M_WAIT8		0x0200		/* 8 wait */
#define	M_WAIT9		0x0240		/* 9 wait */
#define	M_WAIT10	0x0280		/* 10 wait */
#define	M_WAIT11	0x02c0		/* 11 wait */
#define	M_WAIT12	0x0300		/* 12 wait */
#define	M_WAIT13	0x0340		/* 13 wait */
#define	M_WAITF		0x0380		/* fast termination */
#define	M_WAITE		0x03c0		/* external dsack */
#define	M_SPCS		0x0020		/* supervisor */
#define	M_SPCU		0x0010		/* user */
#define	M_AVEC		0x0001		/* 0 = ext rupt, 1 = autovector */


struct	CSOR_			/* CHIP SELECT OPTION REGISTERS (CSOR)  [SIM-4-33]	*/
	{
#ifdef FIRST_BF_8000
	unsint		MODE	:	1;
	unsint		BYTE	:	2;
	unsint		RW		:	2;
	unsint		STRB	:	1;
	unsint		DSACK	:	4;
	unsint		SPACE	:	2;
	unsint		IPL		:	3;
	unsint		AVEC	:	1;
#endif
#ifdef FIRST_BF_0001
	unsint		AVEC	:	1;
	unsint		IPL		:	3;
	unsint		SPACE	:	2;
	unsint		DSACK	:	4;
	unsint		STRB	:	1;
	unsint		RW		:	2;
	unsint		BYTE	:	2;
	unsint		MODE	:	1;
#endif

	};
#define		CSORBT		((uspv)	(IMBASE + 0xA4A))
#define		_CSORBT			((struct CSOR_ *) CSORBT)
#define		CSOR0		((uspv)	(IMBASE + 0xA4E))
#define		_CSOR0			((struct CSOR_ *) CSOR0)
#define		CSOR1		((uspv)	(IMBASE + 0xA52))
#define		_CSOR1			((struct CSOR_ *) CSOR1)
#define		CSOR2		((uspv)	(IMBASE + 0xA56))
#define		_CSOR2			((struct CSOR_ *) CSOR2)
#define		CSOR3		((uspv)	(IMBASE + 0xA5A))
#define		_CSOR3			((struct CSOR_ *) CSOR3)
#define		CSOR4		((uspv)	(IMBASE + 0xA5E))
#define		_CSOR4			((struct CSOR_ *) CSOR4)
#define		CSOR5		((uspv)	(IMBASE + 0xA62))
#define		_CSOR5			((struct CSOR_ *) CSOR5)
#define		CSOR6		((uspv)	(IMBASE + 0xA66))
#define		_CSOR6			((struct CSOR_ *) CSOR6)
#define		CSOR7		((uspv)	(IMBASE + 0xA6A))
#define		_CSOR7			((struct CSOR_ *) CSOR7)
#define		CSOR8		((uspv)	(IMBASE + 0xA6E))
#define		_CSOR8			((struct CSOR_ *) CSOR8)
#define		CSOR9		((uspv)	(IMBASE + 0xA72))
#define		_CSOR9			((struct CSOR_ *) CSOR9)
#define		CSOR10		((uspv)	(IMBASE + 0xA76))
#define		_CSOR10			((struct CSOR_ *) CSOR10)


#define		SZ2K		0x00		/* 2K		A23-A11						*/
#define		SZ8K		0x01		/* 8K		A23-A13						*/
#define		SZ16K		0x02		/* 16K		A23-A14						*/
#define		SZ64K		0x03		/* 64K		A23-A16						*/
#define		SZ128K		0x04		/* 128K		A23-A17						*/
#define		SZ256K		0x05		/* 256K		A23-A18						*/
#define		SZ512K		0x06		/* 512K		A23-A19						*/
#define		SZ1M		0x07		/* 1M		A23-A20						*/
#define		M_PIN7		0x80
#define		M_PIN6		0x40
#define		M_PIN5		0x20
#define		M_PIN4		0x10
#define		M_PIN3		0x08
#define		M_PIN2		0x04
#define		M_PIN1		0x02
#define		M_PIN0		0x01

struct	PINS_
	{
#ifdef FIRST_BF_8000
	unsint				:	8;
	unsint		P7		:	1;

	unsint		P6		:	1;

	unsint		P5		:	1;

	unsint		P4		:	1;

	unsint		P3		:	1;

	unsint		P2		:	1;

	unsint		P1		:	1;

	unsint		P0		:	1;

#endif
#ifdef FIRST_BF_0001
	unsint		P0		:	1;

	unsint		P1		:	1;

	unsint		P2		:	1;

	unsint		P3		:	1;

	unsint		P4		:	1;

	unsint		P5		:	1;

	unsint		P6		:	1;

	unsint		P7		:	1;

	unsint				:	8;
#endif
	};	

/****************************************************************************/
					/* PORT D REGISTERS (PORTD, PDPAR, DDRD)  [SIM-5-15]	*/
#define		PORTD	((ucpv) (IMBASE + 0xC15))	/* data			*/
#define		_PORTD			((volatile struct PINS_ *) (IMBASE + 0xC14))
#define		PDPAR	((ucpv) (IMBASE + 0xC16))	/* assignment	*/
#define		_PDPAR			((struct PINS_ *) (IMBASE + 0xC15))
#define		DDRD	((ucpv) (IMBASE + 0xC17))	/* direction	*/
#define		_DDRD			((struct PINS_ *) (IMBASE + 0xC16))


/****************************************************************************/
					/* PORT E REGISTERS (PORTE, PEPAR, DDRE)  [SIM-4-43]	*/
#define		PORTE	((ucpv) (IMBASE + 0xA11))	/* data			*/
#define		_PORTE			((volatile struct PINS_ *) (IMBASE + 0xA10))
#define		PEPAR	((ucpv) (IMBASE + 0xA17))	/* assignment	*/
#define		_PEPAR			((struct PINS_ *) (IMBASE + 0xA16))
#define		DDRE	((ucpv) (IMBASE + 0xA15))	/* direction	*/
#define		_DDRE			((struct PINS_ *) (IMBASE + 0xA14))


/****************************************************************************/
					/* PORT F REGISTERS (PORTF, PFPAR, DDRF)  [SIM-4-44]	*/
#define		PORTF	((ucpv) (IMBASE + 0xA19))	/* data			*/
#define		_PORTF			((volatile struct PINS_ *) (IMBASE + 0xA18))
#define		PFPAR	((ucpv) (IMBASE + 0xA1F))	/* assignment	*/
#define		_PFPAR			((struct PINS_ *) (IMBASE + 0xA1E))
#define		DDRF	((ucpv) (IMBASE + 0xA1D))	/* direction	*/
#define		_DDRF			((struct PINS_ *) (IMBASE + 0xA1C))


#endif	/*	__sim332_H */

