/*******************************************************************************
**	tpu332.h -- 68332 Tattletale (7,8) Time Processing Unit Definitions
**
**          Copyright 1994 ONSET Computer Corp.  All rights reserved.
**	
**	Thursday, March 31, 1994	2:27 PM	  --jhg
**	
**	NOTE ! The CISR is the only register accessable on a byte basis.  Do not
**	use bit field operators to WRITE ! to any other TPU registers.
*******************************************************************************/

#ifndef		__tpu332_H
#define		__tpu332_H

#include	<tat332.h>

#define		STBY_RAM_SIZE			2048

/*	Module Configuration Register */
#define		RAMMCR			((uspv) (IMBASE + 0xb00))
#define			RSTOP		0x8000	/* 1 = enter low power stop		*/
#define			RASP		0x0080	/* 1 = supervisor, 0 = any		*/


#define		RAMTST			((uspv) (IMBASE + 0xb02))

/*	Standby RAM Base Address Register */
#define		RAMBAR			((uspv) (IMBASE + 0xb04))
#define			RAMDS		0x0001	/* 1 = disabled, 0 = enabled	*/

/****************************************************************************/
#define		M_STOP		0x8000	/* 1 = TPU shut down */
#define		M_PRSCL1_1	0x0000	/* TRC1 prescale divide by 1 */
#define		M_PRSCL1_2	0x2000	/* TRC1 prescale divide by 2 */
#define		M_PRSCL1_4	0x4000	/* TRC1 prescale divide by 4 */
#define		M_PRSCL1_8	0x6000	/* TRC1 prescale divide by 8 */
#define		M_PRSCL2_1	0x0000	/* TRC2 prescale divide by 1 */
#define		M_PRSCL2_2	0x0800	/* TRC2 prescale divide by 2 */
#define		M_PRSCL2_4	0x1000	/* TRC2 prescale divide by 4 */
#define		M_PRSCL2_8	0x1800	/* TRC2 prescale divide by 8 */
#define		M_EMU		0x0400	/* 1 = TPU & RAM in emulation */
#define		M_T2CG		0x0200	/* 1 = TCR2 is gate, 0 = source */
#define		M_STF		0x0100	/* 1 = TPU stopped, 0 = operate */
#define		M_TPUSUPV	0x0080	/* 1 = supervisor, 0 = any */
#define		M_PSCK		0x0040	/* 1 = DIV4, 0 = DIV32 */


struct	TMCR_	/*	TPU MODULE CONFIGURATON REGISTER	(TMCR)  [TPU-2-4]	*/
	{
#ifdef FIRST_BF_8000
	unsint		STOP	:	1;
	unsint		PRSCL1	:	2;
	unsint		PRSCL2	:	2;
	unsint		EMU		:	1;
	unsint		T2CG	:	1;
	unsint		STF		:	1;
	unsint		TPUSUPV	:	1;
	unsint		PSCK	:	1;
	unsint				:	2;
	unsint		IARBID	:	4;
#endif
#ifdef FIRST_BF_0001
	unsint		IARBID	:	4;
	unsint				:	2;
	unsint		PSCK	:	1;
	unsint		TPUSUPV	:	1;
	unsint		STF		:	1;
	unsint		T2CG	:	1;
	unsint		EMU		:	1;
	unsint		PRSCL2	:	2;
	unsint		PRSCL1	:	2;
	unsint		STOP	:	1;
#endif
	};


#define		TMCR			((uspv) (IMBASE + 0xE00))
#define		_TMCR			((struct TMCR_ *) TMCR)


/****************************************************************************/
struct	TICR_	/*	TPU INTERRUPT CONFIGURATION REGISTER (TICR)  [TPU-2-8]	*/
	{
#ifdef FIRST_BF_8000
	unsint				:	5;
	unsint		TINTL	:	3;	/* channel interrupt request level	*/
	unsint		TINTV	:	8;	/* channel interrupt base vector	*/
#endif
#ifdef FIRST_BF_0001
	unsint		TINTV	:	8;	/* channel interrupt base vector	*/
	unsint		TINTL	:	3;	/* channel interrupt request level	*/
	unsint				:	5;
#endif
	};
#define		TICR			((uspv) (IMBASE + 0xE08))
#define		_TICR			((struct TICR_ *) TICR)

#define		ICRIRQL(n)		(((uchar)(n)) << 8)

/****************************************************************************/
			/*	TPU CHANNEL INTERRUPT ENABLE REGISTER (CIER)  [TPU-2-10]	*/
#define		CIER	((uspv) (IMBASE + 0xE0A))

/****************************************************************************/
			/*	TPU CHANNEL INTERRUPT STATUS REGISTER (CISR)  [TPU-2-10]	*/
#define		CISR	((uspv) (IMBASE + 0xE20))


/****************************************************************************/
				/*	TPU CHANNEL FUNCTION SELECT REGISTERS (CFSR) [TPU-2-11]	*/
#define		CFSR		((uspv) (IMBASE + 0xE0C))
#define		CFSR0		((uspv) (IMBASE + 0xe0c))
#define		CFSR1		((uspv) (IMBASE + 0xe0e))
#define		CFSR2		((uspv) (IMBASE + 0xe10))
#define		CFSR3		((uspv) (IMBASE + 0xe12))

#define		QDEC	0x05		/* Quadrature decoder /*
#define		RWTPIN	0x06		/* Read/Write Timer & Pin		*/
#define		SPWM	0x07		/* Sync. Pulse Width Modulation	*/
#define		DIO	0x08		/* Discrete Input/Output		*/
#define		PWM	0x09		/* Pulse Width Modulation		*/
#define		ITC	0x0a		/* Input Capture/Transition Ctr	*/
#define		UART	0x0b		/* UART							*/
#define		FQM	0x0c		/* Frequency Measurement		*/
#define		SQW	0x0d		/* Square Wave					*/
#define		OC	0x0e		/* Output Compare				*/
#define		PPWA	0x0f		/* Period/Pulse Width Accum		*/

#define			FUNSEL(ch,fn)	\
				(* (uspv) (IMBASE + 0xe12 - (((ch) / 4) << 1))) = \
				((* (uspv) (IMBASE + 0xe12 - (((ch) / 4) << 1))) & \
				(~((0xf) << (((ch) % 4) << 2)))) | ((fn) << (((ch) % 4) << 2))



/****************************************************************************/
						/*	TPU HOST SEQUENCE REGISTERS (HSQR) [TPU-2-12]	*/
#define		HSQR	((uspv) (IMBASE + 0xE14))
#define		HSQR0	((uspv) (IMBASE + 0xE14))
#define		HSQR1	((uspv) (IMBASE + 0xE16))

#define			HOSTSEQ(ch,sq)	\
				(* (uspv) (IMBASE + 0xe16 - (((ch) / 8) << 1))) = \
				((* (uspv) (IMBASE + 0xe16 - (((ch) / 8) << 1))) & \
				(~((0x3) << (((ch) % 8) << 1)))) | ((sq) << (((ch) % 8) << 1))
				


/****************************************************************************/
				/*	TPU HOST SERVICE REQUEST REGISTERS (HSRR) [TPU-2-13]	*/
#define		HSRR	((uspv) (IMBASE + 0xE18))
#define		HSRR0	((uspv) (IMBASE + 0xE18))
#define		HSRR1	((uspv) (IMBASE + 0xE1A))

#define			HOSTSERVREQ(ch,rq)	\
				(* (uspv) (IMBASE + 0xe1a - (((ch) / 8) << 1))) = \
				((rq) << (((ch) % 8) << 1))
				
#define			HOSTSERVSTAT(ch)	\
				((* (uspv) (IMBASE + 0xe1a - (((ch) / 8) << 1))) & \
				(((0x3) << (((ch) % 8) << 1))))
				


/****************************************************************************/
						/*	TPU CHANNEL PRIORITY REGISTERS (CPR) [TPU-2-14]	*/
#define		CPR		((uspv) (IMBASE + 0xE1C))
#define		CPR0	((uspv) (IMBASE + 0xE1C))
#define		CPR1	((uspv) (IMBASE + 0xE1E))

#define			CHANPRIOR(ch,pr)	\
				(* (uspv) (IMBASE + 0xe1e - (((ch) / 8) << 1))) = \
				((* (uspv) (IMBASE + 0xe1e - (((ch) / 8) << 1))) & \
				(~((0x3) << (((ch) % 8) << 1)))) | ((pr) << (((ch) % 8) << 1))
				

#define			PARAMRAM(ch)	((uspv) ((IMBASE + 0xf00) + ((ch) << 4)))


/* Channel Priorities */
#define			Disabled			0
#define			LowPrior			1
#define			MiddlePrior			2
#define			HighPrior			3

#define		TPULR			((uspv) (IMBASE + 0xE22))
#define		TPUSGLR			((uspv) (IMBASE + 0xE24))
#define		TPUDCNR			((uspv) (IMBASE + 0xE26))

typedef ushort _PRAM[8];
#define		PRAM			( (_PRAM *) (IMBASE + 0xF00))
#define		PRAM0			( (uspv) (IMBASE + 0xF00))
#define		PRAM1			( (uspv) (IMBASE + 0xF10))
#define		PRAM2			( (uspv) (IMBASE + 0xF20))
#define		PRAM3			( (uspv) (IMBASE + 0xF30))
#define		PRAM4			( (uspv) (IMBASE + 0xF40))
#define		PRAM5			( (uspv) (IMBASE + 0xF50))
#define		PRAM6			( (uspv) (IMBASE + 0xF60))
#define		PRAM7			( (uspv) (IMBASE + 0xF70))
#define		PRAM8			( (uspv) (IMBASE + 0xF80))
#define		PRAM9			( (uspv) (IMBASE + 0xF90))
#define		PRAM10			( (uspv) (IMBASE + 0xFA0))
#define		PRAM11			( (uspv) (IMBASE + 0xFB0))
#define		PRAM12			( (uspv) (IMBASE + 0xFC0))
#define		PRAM13			( (uspv) (IMBASE + 0xFD0))
#define		PRAM14			( (uspv) (IMBASE + 0xFE0))
#define		PRAM15			( (uspv) (IMBASE + 0xFF0))

#endif	/*	__tpu332_H */

