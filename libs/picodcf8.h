/*******************************************************************************
**	PicoDCF8.h -- PicoDOS Interface definitions for Persistor CF8
**	
**	First release:		Monday, October 14, 1996
**	Latest release:		Thursday, July 17, 1997
*****************************************************************************
**	
**	Developed by: John H. Godley dba Peripheral Issues
**	P.O.Box 543, Mashpee, MA 02649-0543 USA
**	issues@periph.com - http://www.periph.com
**	
**	Copyright (C) 1996 Peripheral Issues 
**	All rights reserved.
**	
*****************************************************************************
**	Modified, PicoDOS 1.32, Monday, January 20, 1997 --jhgodley@periph.com
**	
**		Older versions (pre-5.2C) of the Aztec compiler wouldn't allow the
**		double slash comments for preprocessor declarations. Changed then
**		all to slash-asterisk pairs.
**	
**	Modified, PicoDOS 1.33, Monday, March 3, 1997 --jhgodley@periph.com
**	
**		Renamed from pDOS8.h to PicoDCF8.h and created a new pDOS8.h which
**		simply includes the new PicoDCF8.h. Though this may cause some 
**		initial confusion, it will ultimately allow various versions of
**		PicoDOS to coexist within the same development environment.
**		
**		This should have no noticable effect on existing code, though you
**		should make an effort to create new source modules using the new
**		PicoDCF8.h include file.
**	
**		Renamed from ResetToPDOS8 to ResetToPicoDOS, and added ResetToPDOS8 
**		macro to old pDOS8.h to support earlier code.
**	
**		Renamed from ResetToPDOS8 to ResetToPicoDOS, and added ResetToPDOS8 
**		macro to old pDOS8.h to support earlier code.
**	
**		Modified InitCF8() to properly return -1 (as opposed to crashing) if
**		no PicoDOS image is found in high flash.
*******************************************************************************/
#ifndef	__PicoDCF8_h
#define	__PicoDCF8_h

#ifndef PRECOMPHDRS 
	#include	<tt8lib.h>
	#include	<stdio.h>
	#include	<stdlib.h>
	#include	<errno.h>
#endif

#define	MDOS8MEM		0x7000		/* 28KB required for CF8 PicoDOS */

#define	CF8StdCS		2			/* jumper defaults to chip select 2 */
#define	CF8AltCS1		1			/* cut and jump to chip select 1 */
#define	CF8AltCS10		10			/* cut and jump to chip select 10 */
#define	CF8StdAddr		0x800000	/* gotta be somewhere */

#ifdef __GNUC__
#define initpd()\
    ({short __r;\
      __asm__ volatile ("    lea 0x2bcf8,%%a0;\
                             movew #1000,%0;\
                          0: cmpil #0x5069446f,%%a0@;\
                             bne 1f;\
                             cmpil #0x73436638,%%a0@(4);\
                             beq 2f;\
                             1: addql #2,%%a0;\
                             dbra %0,0b;\
                             bra 3f;\
                          2: lea %%a0@(8),%%a0;\
                             jsr %%a0@;\
                             moveq #0,%0;\
                             3: nop;" : "=d" (__r) : : "%a0", "%a1"); __r;})
#else

short initpd(void) = 
	{	0x41F9,0x0002,0xBCF8,0x303C,0x03E8,0x0C90,0x5069,0x446F,
		0x660A,0x0CA8,0x7343,0x6638,0x0004,0x6708,0x5488,0x51C8,
		0xFFEA,0x6008,0x41E8,0x0008,0x4E90,0x7000,0x4E71 };
#endif /* __GNUC__ */

#define	InitCF8(cs,ad)						\
	{										\
	if ((errno = initpd()) == 0)			\
		if ((errno = initcf(cs, ad)) == 0) {\
			extern char *_mtop;				\
			_mtop -= MDOS8MEM; 				\
			initfs(calloc, free, &errno); }	\
	}

#ifdef __GNUC__
#define ResetToPicoDOS()\
    __asm__("moveal #0x2bcf8,%%a0; jmp %%a0@"::: "%a0")
#else
void ResetToPicoDOS(void) = { 0x4EF9, 0x0002, 0xBCF8 };
#endif

#define	CFOPEN_MAX		10			/* maximum open files (not incl. con) */

/*
**	E R R O R   C O D E S
*/
enum
	{
	PiDosNoError		= 0,
	PiDosNoHardware		= 100,		// no hardware detected
	PiDosNoMedia,					// no media found
	PiDosInvalRequest,				// invalid argument or request
	PiDosNoMemory,					// not enough memory
	PiDosOpenFailure,				// couldn't open file
	PiDosRequestFailed				// couldn't complete request
	};

enum {
	_u_initcf = 0x7000, _u_initfs, _u_initargs, 
	_u_open, _u_close, _u_read, _u_write, _u_lseek, 
	_u_ioctl, _u_isatty, _u_rename, _u_unlink,
	_u_execstr, _u_picodosver, _u_pdcfpro, _u_pdcfinfo
	};

enum {
    _V2CardPower = 32000,       // turn on (1) or off (0) card and buffers
    _V2CardReset                        // reset the flash card
};
#define V2CardPower(onoff)              initcf(_V2CardPower, onoff)
#define V2CardReset(onoff)              initcf(_V2CardReset, 0)

#ifdef AZTEC_C
	#pragma regcall (d0 initcf (a0, d1))
		short	initcf(short, ulong) = { _u_initcf, 0x4E4C };
	#pragma regcall (initfs (d1, a0, a1))
		void	initfs(void *, void *, void *) = { _u_initfs, 0x4E4C };
	#pragma regcall (d0 initargs (a0))
		int		initargs(char ***) = { _u_initargs, 0x4E4C };
	#pragma regcall (d0 execstr (a0))
		int		execstr(char *) = { _u_execstr, 0x4E4C };
	#pragma regcall (d0 picodosver ())
		char	*picodosver(void) = { _u_picodosver, 0x4E4C };
	#pragma regcall (pdcfpro (a0))
		void	pdcfpro(void *) = { _u_pdcfpro, 0x4E4C };
	#pragma regcall (d0 pdcfinfo (d1, a0, a1))
		short	pdcfinfo(char *, long *, long *) = { _u_pdcfinfo, 0x4E4C };
#endif	/* AZTEC_C */

#ifdef __GNUC__

#define initcf(a, b)\
    ({short __a = (a); ulong __b = (b); short __r;\
      __asm__ volatile ("movew %1,%%a0;\
                         movel %2,%%d1;\
                         clrl %%d0;\
                         trap #12;\
                         movew %%d0,%0"\
                         : "=d" (__r) : "g" (__a), "g" (__b)\
                         : "%d0", "%d1", "%a0");\
      __r;})

#define initfs(a, b, c)\
    ({void *__a = (a), *__b = (b), *__c = (c);\
      __asm__ volatile ("movel %0,%%d1;\
                         movel %1,%%a0;\
                         movel %2,%%a1;\
                         moveq #1,%%d0;\
                         trap #12"\
                         : : "g" (__a), "g" (__b), "g" (__c)\
                         : "%d0", "%d1", "%a1", "%a0");})

#define initargs(a)\
    ({char ***__a = (a); int __r;\
      __asm__ volatile ("movel %1,%%a0;\
                         moveq #2,%%d0;\
                         trap #12;\
                         movel %%d0,%0;"\
                         : "=d" (__r) : "g" (__a)\
                         : "%d0", "%a0"); __r;})

#define execstr(a)\
    ({char *__a = (a); int __r;\
      __asm__ volatile ("movel %1,%%a0;\
                         moveq #12,%%d0;\
                         trap #12;\
                         movel %%d0,%0;"\
                         : "=d" (__r) : "g" (__a)\
                         : "%d0", "%a0"); __r;})

#define picodosver()\
    ({char *__r;\
      __asm__ volatile ("moveq #13,%%d0;\
                         trap #12;\
                         movel %%d0,%0;"\
                         : "=g" (__r) :\
                         : "%d0"); __r;})

#define pdcfinfo(a, b, c)\
    ({char *__a = (a); long *__b = (b); long *__c = (c); short __r;\
      __asm__ volatile ("movel %1,%%d1;\
                         movel %2,%%a0;\
                         movel %3,%%a1;\
                         moveq #15,%%d0;\
                         trap #12;\
                         movew %%d0,%0;"\
                         : "=g" (__r) : "g" (__a), "g" (__b), "g" (__c)\
                         : "%d0", "%d1", "%a0", "%a1"); __r;})

#endif /* __GNUC__ */

#ifdef __MWERKS__
  #ifndef _PICODOS_KERNEL_
	#pragma parameter __d0 initcf (__a0, __d1)
		short	initcf(short, ulong) = { _u_initcf, 0x4E4C };
	#pragma parameter initfs (__d1, __a0, __a1)
		void	initfs(void *, void *, void *) = { _u_initfs, 0x4E4C };
	#pragma parameter __d0 initargs (__a0)
		int		initargs(char ***) = { _u_initargs, 0x4E4C };
	#pragma parameter __d0 execstr (__a0)
		int		execstr(char *) = { _u_execstr, 0x4E4C };
	#pragma parameter __d0 picodosver ()
		char	*picodosver(void) = { _u_picodosver, 0x4E4C };
	#pragma parameter pdcfpro (__a0)
		void	pdcfpro(void *) = { _u_pdcfpro, 0x4E4C };
	#pragma parameter __d0 pdcfinfo (__d1, __a0, __a1)
		short	pdcfinfo(char *, long *, long *) = { _u_pdcfinfo, 0x4E4C };
  #endif	/* _PICODOS_KERNEL_ */
#endif	/* __MWERKS__ */

#endif	/*	__PicoDCF8_h */
