/*******************************************************************************
**	tt8.h -- Tattletale Model 8 Definitions
**
**          Copyright 1994 ONSET Computer Corp.  All rights reserved.
**	
**	Sunday, November 14, 1993
**	
**	94/07/04 --jhg
**	Changed the default INTERRUPT REQUEST LEVELS 
**		from:	TPU=3, PIT=4, SCI=5, QSPI=6
**		to	:	PIT=1, TPU=3, SCI=4, QSPI=5
**	We want low priority for pseudo-static RAM refresh from periodic interrupt.
**	These are just the defaults used by InitTT8(), users can adjust priority in
**	the body of their programs.
*******************************************************************************/

#ifndef		__tt8_H
#define		__tt8_H


/*
**	MODEL 8 CHIP SELECTS
*/
#define	FlashBase		_CSBARBT
#define	FlashOpts		_CSORBT
#define	FlashRdBase		_CSBAR7
#define	FlashRdOpts		_CSOR7

#define	RamOEBase		_CSBAR3
#define	RamOEOpts		_CSOR3
#define	RamLoBase		_CSBAR5
#define	RamLoOpts		_CSOR5
#define	RamHiBase		_CSBAR4
#define	RamHiOpts		_CSOR4

#define	PicBaseAddr		0xE00000
#define	PicPort			((ucpv) PicBaseAddr)
#define	PicAddrSize		SZ2K
#define	PicRdBase		CSBAR8
#define	PicRdOpts		CSOR8
#define	PicRdPar		((*CSPAR1 & M_CS8) >> 4)
#define	PicWrBase		CSBAR9
#define	PicWrOpts		CSOR9
#define	PicWrPar		((*CSPAR1 & M_CS9) >> 6)

/*
**	MODEL 8 VBR DRIVER POINTERS
*/
enum
	{
	  SCI_DRIVER = 16			/* 0x40 -- Serial Controller Interface */
	, TS_DRIVER					/* 0x44 -- TPU Auxillery Serial Channels */
	, PIC_DRIVER				/* 0x48 -- PIC-16C64 RTC & Power Management */
	, TPU_DRIVER				/* 0x4C -- TPU Function Calls */
	, FLASH_DRIVER				/* 0x50 -- Flash Programming Functions */
	, AD_DRIVER					/* 0x54 -- MAX-186 QSPI Functions */
	};

/*
**	INTERRUPT ARBITRATION ID NUMBERS
*/
enum
	{
	  MCR_INT_ARB_ID = 8
	, TPU_INT_ARB_ID
	, QSM_INT_ARB_ID
	};

/*
**	INTERRUPT VECTOR NUMBERS
*/
enum
	{
	  TPU_INT_VECTOR = 64			/* MUST BE ALIGNED ON 16 BYTE BOUND !!! */
	, PIT_INT_VECTOR = TPU_INT_VECTOR + 16
	, avail_INT_VECTOR
	, SCI_INT_VECTOR		/* \  these two must be kept together */
	, QSPI_INT_VECTOR		/* /  and should be evenly aligned (I think!)  */
	
	};

/*
**	INTERRUPT REQUEST LEVELS
*/
enum
	{
	   PIT_INT_REQ_LEVEL = 1
	,  TPU_INT_REQ_LEVEL = 3
	,  SCI_INT_REQ_LEVEL
	,  QSPI_INT_REQ_LEVEL
	
	};


#endif	/*	__tt8_H */
