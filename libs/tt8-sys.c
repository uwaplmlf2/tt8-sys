/*
** $Id$
**
** GNU-newlib support functions for the Tattletale Model 8 (TT8)
** MC68332 controller board.
**
*/
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <tat332.h>
#include <tt8.h>
#include <tt8lib.h>

/*
** These pointers are initialized in Onset's startup function.
*/
extern void* 	_mtop;
extern void* 	_mbot;
extern void* 	_mcur;

void*
sbrk(long n_bytes)
{
    void	*base;
    
    if((_mcur + n_bytes) <= _mtop)
    {
	base = _mcur;
	_mcur += n_bytes;
	return base;
    }
    else
    {
	errno = ENOMEM;
	return ((void*)-1);
    }
}

    
int
gettimeofday(struct timeval *tv, struct timezone *tz)
{
    time_tt	*now = ttmnow();
    
    tv->tv_sec = now->secs;
    tv->tv_usec = now->ticks*1000000L/GetTickRate();
    
    return 0;
}

int
fstat(int fd, struct stat *s)
{
    s->st_mode = S_IFCHR;
    s->st_blksize = 0;
    return 0;
}

int
getpid(void)
{
    return 0;
}

int
kill(int pid, int sig)
{
    return 0;
}
