/*******************************************************************************
**	tt8pic.h -- Tattletale Model 8 PIC Parallel Slave Port Definitions
**
**          Copyright 1994 ONSET Computer Corp.  All rights reserved.
**	
**	Wednesday, February 16, 1994
*******************************************************************************/

#ifndef		__tt8pic_H
#define		__tt8pic_H

#include	<tat332.h>
#include	<sim332.h>
#include	<dio332.h>

void  PicInit(long sysfreq);

short PicGetRevs(void);

short PicReadRF(short addr);
short PicWriteRF(short addr, short data);
short PicAddRF(short addr, short data);
short PicAndRF(short addr, short data);
short PicOrRF(short addr, short data);
short PicXorRF(short addr, short data);

short PicWriteSEE(short data);
short PicSetAddrSEE(ushort addr);
short PicReadSEE(void);
short PicGetEESize(void);

short PicGetTmr1(ushort *ticks, short raw);
short PicGetSecs(ulong *seconds);
short PicGetTime(ulong *seconds, ushort *ticks);
short PicGetAlarm(ulong *seconds);
short PicSetTime(ulong seconds);
short PicSetAlarm(ulong seconds);

short PicAckPerAlrm(void);
short PicAckCmpAlrm(void);
short PicAckLowBat(void);
short PicAckPB4567Chg(void);
short PicGetIrqAddr(void);


typedef enum
	{
	  picOK = 0
	, picWritePromptTimeout		= -1
	, picIrq5ReleaseTimeout		= -2
	, picReadPromptTimeout		= -3
	, picIrq3ReleaseTimeout		= -4
	, picCmdErrPicNotRespond	= -5
	
	} PSPErr;


#define		PICPORTA	0x05
#define		PICPORTB	0x06
#define		PICPORTC	0x07
#define		PICPORTD	0x08
#define		PICPORTE	0x09

#define		PICTRISA	0x85
#define		PICTRISB	0x86
#define		PICTRISC	0x87
#define		PICTRISD	0x88
#define		PICTRISE	0x89

#endif	/*	__tt8pic_H */
