/*******************************************************************************
**	userio.h -- Tattletale Model 8 User I/O Interface Function Prototypes
**
**	Copyright (C) 1991-94 ONSET Computer Corp.  --jhg		All rights reserved.
**
**	Saturday, January 26, 1991
*******************************************************************************/


extern short	QueryDateTime(ptr prompt, short defTime, struct tm *tm);
extern short	QueryYesNo(ptr prompt, short defYes);
extern short	QueryChar(ptr prompt, short defChar, ptr scanSet, char *reply);
extern short	QueryNum(ptr prompt, ptr defFmt, ptr scanFmt, ulong *value);
extern short	InputLine(ptr linebuf, short linelen);
extern short	kbhit(void);
extern short	kbflush(void);

