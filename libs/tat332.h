/*******************************************************************************
**	tat332.h -- 68332 Tattletale (7,8) Hardware Definitions
**
**          Copyright 1994 ONSET Computer Corp.  All rights reserved.
**	
**	94/02/11 --jhg
**		Adapted from TT7 "tt7.h"
*******************************************************************************/

#ifndef		__tat332_H
#define		__tat332_H


#ifdef THINK_C
	#define FIRST_BF_8000	/* bit field assignment 8000,4000,2000,...,0001 */
#endif	/* THINK_C */
#if defined(AZTEC_C)
	#define FIRST_BF_0001	/* bit field assignment 0001,0002,0004,...,8000 */
#endif	/* AZTEC_C */
#if defined(__MWERKS__) || defined(__GNUC__)
	#define FIRST_BF_8000	/* bit field assignment 8000,4000,2000,...,0001 */
#endif	/* __MWERKS__ */

typedef unsigned char		uchar;
typedef volatile uchar		*ucpv;
#ifndef _SYS_TYPES_H
typedef unsigned short		ushort;
#endif
typedef unsigned int		unsint;
typedef volatile ushort		*uspv;
typedef unsigned long		ulong;
typedef volatile ulong		*ulpv;
typedef char 				*ptr;
typedef	void				(*vfptr)(void);
//#ifndef __MWERKS__
typedef enum {FALSE, TRUE}	bool;
//#endif	/* __MWERKS__ */


#define IMBASE			0xFFFFF000
#define	FCRYSTAL		40000L

/*
**	MASKS FOR SETTING AND CLEARING BITS IN INTERNAL REGISTERS
*/
#define	SET		0xffff
#define	CLR		0

#define	HI		0xffff
#define	LO		0
#define	NC		0xffff

#define	BUS		0xffff
#define	IO		0

#define	OUTP	0xffff
#define	INP		0

#define	ALLIN	0x00
#define	ALLOUT	0xff

#endif	/*	__tat332_H */

